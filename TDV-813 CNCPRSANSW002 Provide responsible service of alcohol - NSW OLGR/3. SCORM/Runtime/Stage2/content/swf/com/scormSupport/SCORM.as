﻿
package com.scormSupport {

	import flash.external.*;
	
	public class SCORM {
		
		private var __connectionActive:Boolean = false,
					__debugActive:Boolean = true;
	
	
		public function SCORM() {
		
			var is_EI_available:Boolean = ExternalInterface.available,
				wrapperFound:Boolean = false,
				debugMsg:String = "Initializing SCORM class. Checking dependencies: ";
				
			if(is_EI_available){
				
				debugMsg += "ExternalInterface.available evaluates true. ";
				
				wrapperFound = Boolean(ExternalInterface.call("scormSupport.SCORM.isAvailable"));
				debugMsg += "SCORM.isAvailable() evaluates " +String(wrapperFound) +". ";
				
				if(wrapperFound){
					
					debugMsg += "SCORM class file ready to go!  :) ";
				
				} else {
	
					debugMsg += "The required JavaScript SCORM API wrapper cannot be found in the HTML document.  Course cannot load.";
				
				}
				
			} else {
				
				debugMsg += "ExternalInterface is NOT available (this may be due to an outdated version of Flash Player).  Course cannot load.";
				
			}
	
			__displayDebugInfo(debugMsg);
		
		}
	
	
		
		// --- public functions --------------------------------------------- //
		
	
		public function set debugMode(status:Boolean):void {
			this.__debugActive = status;
		}
	
		public function get debugMode():Boolean {
			return this.__debugActive;
		}
	
		public function connect():Boolean {
			__displayDebugInfo("scormSupport.SCORM.connect() called from class file");
			return __connect();
		}
		
		public function disconnect():Boolean {
			return __disconnect();
		}
		
		public function get(param:String):String {
			var str:String = __get(param);
			__displayDebugInfo("public function get returned: " +str);
			return str;
		}
		
		public function set(parameter:String, value:String):Boolean {
			return __set(parameter, value);
		}
		
		public function save():Boolean {
			return __save();
		}
		

	
		// --- private functions --------------------------------------------- //
		
		
		private function __connect():Boolean {
			
			var result:Boolean = false;
			if(!__connectionActive){
				
				var eiCall:String = String(ExternalInterface.call("scormSupport.SCORM.init"));
				result = __stringToBoolean(eiCall);
				
				if (result){
					__connectionActive = true;
				} else {
					var errorCode:int = __getDebugCode();
					if(errorCode){
						var debugInfo:String = __getDebugInfo(errorCode);
						__displayDebugInfo("scormSupport.SCORM.init() failed. \n"
										  +"Error code: " +errorCode +"\n"
										  +"Error info: " +debugInfo);
					} else {
						__displayDebugInfo("scormSupport.SCORM.init failed: no response from server.");
					}
				}
			} else {
				  __displayDebugInfo("scormSupport.SCORM.init aborted: connection already active.");
			}
			
			__displayDebugInfo("__connectionActive: " +__connectionActive);
			
			return result;
		}
	
		
		private function __disconnect():Boolean {
			
			var result:Boolean = false;
			if(__connectionActive){
				var eiCall:String = String(ExternalInterface.call("scormSupport.SCORM.quit"));
				result = __stringToBoolean(eiCall);
				if (result){
					__connectionActive = false;
				} else {
					var errorCode:int = __getDebugCode();
					var debugInfo:String = __getDebugInfo(errorCode);
					__displayDebugInfo("scormSupport.SCORM.quit() failed. \n"
									  +"Error code: " +errorCode +"\n"
									  +"Error info: " +debugInfo);
				}
			} else {
				__displayDebugInfo("scormSupport.SCORM.quit aborted: connection already inactive.");
			}
			return result;
		}
		
		
		private function __get(parameter:String):String {
		
			var returnedValue:String = "";
			
			if (__connectionActive){
				
				returnedValue = String(ExternalInterface.call("scormSupport.SCORM.get", parameter));
				var errorCode:int = __getDebugCode();
	
				//GetValue returns an empty string on errors
				//Double-check errorCode to make sure empty string
				//is really an error and not field value
				if (returnedValue == "" && errorCode != 0){
					var debugInfo:String = __getDebugInfo(errorCode);
					__displayDebugInfo("scormSupport.SCORM.get(" +parameter +") failed. \n"
									  +"Error code: " +errorCode +"\n"
									  +"Error info: " +debugInfo);
				}
			} else {
				__displayDebugInfo("scormSupport.SCORM.get(" +parameter +") failed: connection is inactive.");
			}		
			return returnedValue;
		}
	
		
		private function __set(parameter:String, value:String):Boolean {
		
			var result:Boolean = false;
			if (__connectionActive){
				var eiCall:String = String(ExternalInterface.call("scormSupport.SCORM.set", parameter, value));
				result = __stringToBoolean(eiCall);
				if(!result){
					var errorCode:int = __getDebugCode();
					var debugInfo:String = __getDebugInfo(errorCode);
					__displayDebugInfo("scormSupport.SCORM.set(" +parameter +") failed. \n"
									  +"Error code: " +errorCode +"\n"
									  +"Error info: " +debugInfo);
				}
			} else {
				__displayDebugInfo("scormSupport.SCORM.set(" +parameter +") failed: connection is inactive.");
			}
			return result;
		}
			
			
		private function __save():Boolean {
			
			var result:Boolean = false;
			if(__connectionActive){
				var eiCall:String = String(ExternalInterface.call("scormSupport.SCORM.save"));
				result = __stringToBoolean(eiCall);
				if(!result){
					var errorCode:int = __getDebugCode();
					var debugInfo:String = __getDebugInfo(errorCode);
					__displayDebugInfo("scormSupport.SCORM.save() failed. \n"
									  +"Error code: " +errorCode +"\n"
									  +"Error info: " +debugInfo);
				}
			} else {
				__displayDebugInfo("scormSupport.SCORM.save() failed: API connection is inactive.");
			}
			return result;
		}
			
	
		// --- debug functions ----------------------------------------------- //
			
		private function __getDebugCode():int {
			var code:int = int(ExternalInterface.call("scormSupport.SCORM.debug.getCode"));
			return code;
		}
			
		private function __getDebugInfo(errorCode:int):String {
			var result:String = String(ExternalInterface.call("scormSupport.SCORM.debug.getInfo", errorCode));
			return result;
		}
		
		private function __getDiagnosticInfo(errorCode:int):String {
			var result:String = String(ExternalInterface.call("scormSupport.SCORM.debug.getDiagnosticInfo", errorCode));
			return result;
		}
	
		private function __displayDebugInfo(msg:String):void {
			if(__debugActive){
				//trace(msg);
				ExternalInterface.call("scormSupport.UTILS.trace", msg);
			}
		}
		
		private function __stringToBoolean(value:*):Boolean {
			
			var t:String = typeof value;
			
			switch(t){
				
			   case "string": return (/(true|1)/i).test(value);
			   case "number": return !!value;
			   case "boolean": return value;
			   case "undefined": return null;
			   default: return false;
			
			}
			
		}
		

		
	} // end SCORM class
} // end package