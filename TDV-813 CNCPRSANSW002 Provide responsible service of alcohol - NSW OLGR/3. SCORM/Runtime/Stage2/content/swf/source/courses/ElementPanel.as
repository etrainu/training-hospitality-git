package source.courses
{
	import flash.display.*;
	import flash.filters.*;
	import flash.events.*;
	import source.general.*;

	public class ElementPanel extends ElementTableColumn
	{
		public static var margin_array:Array = ["wide","medium","none"];
		public static var margin_width:int = 10;
		public static var margin_height:int = 10;
		public static var padding_width_array:Array = [32,16,0];
		public static var padding_height_array:Array = [16,8,0];
		public var margin_index:int = 1;

		public var margin_sprite:Sprite;

		public override function Setup (_xml:XML, _slide:CourseSlide):void
		{
			super.Setup (_xml, _slide);

			var margin:String = _xml.@margin;
			margin_index = margin_array.indexOf (margin);
			if (margin_index == -1) margin_index = 1; // default medium

			element_margin_top = margin_height;
			element_margin_bottom = margin_height;
			element_margin_left = margin_width;
			element_margin_right = margin_width;

			element_padding_width = padding_width_array[margin_index];
			element_padding_height = padding_height_array[margin_index];

			SetEnabled (String (xml.@enabled) == "true");

			has_label = true;
		}
		public override function DoLayout (layout_width:int, layout_height:int, fix_width:Boolean=false, fix_height:Boolean=false):Boolean
		{
			super.DoLayout (layout_width, layout_height);

			if (fix_width) element_width = layout_width;

			var graphics:Graphics = element_sprite.graphics;
			graphics.clear();
			graphics.lineStyle (1, 0xA0A0A0, 1, true);
			graphics.beginFill (0xFFFFFF);
			graphics.drawRoundRect (0, 0, element_width, element_height, 12, 12);
			graphics.endFill();
			element_sprite.filters = [new DropShadowFilter (1,45,0,0.5,8,8,1,4)];

			return true;
		}
		public override function DocumentElement ():String
		{
			return DocumentBlock (group_array);
		}
	}
}
