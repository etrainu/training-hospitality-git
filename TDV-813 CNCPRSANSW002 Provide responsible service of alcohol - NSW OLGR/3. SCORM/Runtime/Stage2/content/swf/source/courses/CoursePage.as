package source.courses
{
	import flash.display.*;
	import flash.events.*;
	
	import source.custom.*;
	import source.general.*;
	
	public class CoursePage extends Sprite
	{
		public var page_xml:XML;
		
		public function CoursePage (xml:XML)
		{
			page_xml = xml;
		}
		public function NavigateForward (event:Event):Boolean
		{
			return false;
		}
		public function NavigateBack (event:Event):Boolean
		{
			return false;
		}
		public function OnEnterFrame (event:Event):void
		{

		}
		public function UnloadAndStop ():void
		{

		}
	}
}
