﻿package source.courses
{
	import flash.display.*;
	import flash.events.*;
	import flash.text.*;
	import flash.ui.*;
	import flash.utils.*;
	import source.general.*;

	public class Assessment extends CoursePage
	{
		// skin
		public static var submit_xpos:int = 0;
		public static var submit_ypos:int = 0;
		public static var message_xpos:int = 0;
		public static var message_ypos:int = 0;
		public static var message_size:int = 16;
		public static var message_color:uint = 0x303030;
		public static var message_text_pass:String = "Well done!<br><br>You have passed";
		public static var message_text_fail:String = "You scored $correct out of $total<br><br>$pass is required to pass<br><br>Please try again"
		public static var message_button_width:int = 140;
		public static var buttons_xpos:int = 0;
		public static var buttons_ypos:int = 0;
		public static var button_gap:int = 34;
		// assessment
		public var questions:Array = [];
		public var reserve_matrix:Array = [];
		public var reserve_questions:Array = [];
		public var current_question_index:int = 0;
		public var pass_threshold:Number = 1.0;
		public var question_count:int = 0;
		public var number_correct:int = 0;
		public var question_start_time:int = 0;
		// sprites
		public var question_sprite:Sprite
		public var results_sprite:Sprite
		public var activity_sprite:Sprite
		// final navigation
		public var button_submit_sprite:Sprite;
		public var button_main_menu_sprite:Sprite;
		public var button_redo_stage_sprite:Sprite;
		public var button_retry_sprite:Sprite;
		// buttons
		public static var button_submit_class:*;
		public static var button_retry_class:*;
		public static var button_redo_stage_class:*;
		public static var button_main_menu_class:*;
		// functions
		public static var function_on_question:Function;
		public static var function_success:Function;
		public static var function_main_menu:Function;
		public static var function_redo_stage:Function;
		// activity mode
		public var scorm_mode:Boolean = true;
		public var activity_mode:Boolean = false;
		public static var activity_xpos:int = 0;
		public static var activity_ypos:int = 0;
		public static var activity_size:int = 14;
		public static var activity_color:uint = 0x606060;
		public static var activity_wrong:Array = ["Wrong answer ... try again", "Still wrong ... keep trying"];
		public static var activity_correct:Array = ["Correct ... well done!", "Correct again!<br><br>Congratulations, you have passed"];
		public var activity_wrong_count:int = 0;
		public var assessment_complete:Boolean = false;
		
		public var is_corrects:Boolean;
		public var popup:feedbackpopup = new feedbackpopup();
		public var clip:submitBtnNew = new submitBtnNew();
		public var clip_1:submitBtnNew = new submitBtnNew();
		public var correctFeedbackArr:Array = new Array("It is everyone’s responsibility.", "You must record the name of injured person, location of the injured person, name of the client and the status of the injured person.", "\n1. MTI = Medical treatment injury\n2. LTI = Lost Time Injury\n3. ISES = Information Systems for Environment and Safety\n4. RTWC =Return to work Coordinator","If a worker is taken from a worksite by ambulance call the Group Injury Management Coordinator ASAP.","On ISES you can find accident details, including any equipment involved or environmental damage, injury details, root causal factors identified, and outcomes, recommendations or actions.","The accident details, and as much information from the other sections as possible, must be entered into ISES within 48 hours.","We need to we know how we are performing, know what types of injuries are occurring, find the root causes of accidents so we can prevent them, and meet our legal obligations for recording accidents.","Reporting within 48 hours to the insurer, advising the employees of their rights, working with the insurer, and making sure we have workers compensation insurance are all obligations we must meet as an employer.","The employee has the right to be involved in the injury management process, is obliged to provide reports on their medical progress, must communicate with their employer, report their injuries and also has the right to information about the workers compensation process.","You need to fill in all the sections that are relevant to the accident, or make every effort to do so.","A tetanus shot is not medical treatment.  For a complete definition of what a Medical Treatment Injury is please refer to the Accident Reporting, Recording and Investigation Policy.","We must look for solutions for near miss events so we can provide a safer work environment and eliminate possibility of future injuries from the same occurrence.","The information recorded on the form relates directly with ISES","Recommendations can come from the injured employee, other employees, employee representatives, Consultants, the Account Manager, Supervisors or Senior Managers, and in some cases from workplace regulatory bodies such as WorkCover.","All the actions that must be completed must be entered into ISES first and then marked as \"Completed\".","Every injured worker has the right to choose if they want to make a Workers Compensation claim.","If an employee is able to undertake any form of work the Doctor will provide a medical certificate that states they can undertake duties with certain restrictions.","Although this may occur if suitable duties cannot be found, usually employee can return to our office or the client site to undertake duties that are within the limitations defined by the Doctor.","The RTWC will assist and oversee all parties so the employee can return to work efficiently and effectively.","An injury is reportable to the insurer when the employee needs to see a doctor and the doctor issues a medical certificate");
		
		//public var incorrectFeedbackArr:Array = new Array("It is everyone’s responsibility.", "You must record the name of injured person, location of the injured person, name of the client and the status of the injured person.");
		
		public static var QuestionClassCount:Class = Question;

		public function Assessment (ass_xml:XML, flat_mode:Boolean=false)
		{
			
			addChild(clip);
			clip.alpha = 0.7;
			clip.x = 307;
			clip.y = 350;
			clip.buttonMode = false;
					
					
					
			super (ass_xml);

			questions = [];
			question_count = 0;
			number_correct = 0;
			current_question_index = 0;
			pass_threshold = (Number (ass_xml.@pass) / 100);
			var question_xml_list:XMLList = ass_xml.question;
			for each (var question_xml:XML in question_xml_list){
				QuestionXML (question_xml, flat_mode);}

			var mode:String = ass_xml.@mode;
			scorm_mode = (mode.indexOf ("scorm") != -1);
			activity_mode = (mode.indexOf ("activity") != -1);
			if (scorm_mode){
				Scorm.AssessmentStart (ass_xml);}
		}
		public function QuestionXML (ques_xml:XML, flat_mode:Boolean=false):void
		{
			var question:Question;
			var question_type:String = ques_xml.@type;
			if (question_type == "multiple choice") question = new QuestionMultipleChoice (ques_xml);
			else if (question_type == "match text text") question = new QuestionMatchTextText (ques_xml);
			else if (question_type == "match text image") question = new QuestionMatchTextImage (ques_xml);
			else if (question_type == "drag drop text") question = new QuestionDragDropText (ques_xml);
			else if (question_type == "drag drop image") question = new QuestionDragDropImageFixed (ques_xml);
			//else if (question_type == "drag drop image") question = new QuestionDragDropImageFlow (ques_xml);
			if (question){
				question.activity_handler = ActivityHandler;
				var reserve:String = ques_xml.@reserve;
				if (reserve == "" || flat_mode){
					questions.push (question);
					reserve_matrix.push (new Array);
					question_count++;}
				else if (reserve == "any"){
					reserve_questions.push (question);}
				else if (reserve == "matrix"){
					if (reserve_matrix.length){
						var matrix_array:Array = reserve_matrix[reserve_matrix.length-1];
						matrix_array.push (question);}}}
		}
		public function AskQuestions ():void
		{
			DisplayCurrentQuestion();
			UpdateNavigation();
		}
		public function JumpToQuestion (ord:String):void
		{
			for (var i:int=0; i<questions.length; i++){
				var question:Question = questions[i];
				if (question.question_ord == ord){
					current_question_index = i;
					AskQuestions();
					break;}
				else {
					question.RenderQuestion ();
					question.Cheat();}}
		}
		public function DisplayCurrentQuestion():void
		{
			activity_wrong_count = 0;
			var question:Question = questions[current_question_index];
			if (!question) return;
			question_sprite = question.RenderQuestion ();
			this.addChild (question_sprite);

			if (question.sound_url){
				var sound_path:String = Course.ResolveMediaPath ("sounds", question.sound_url);
				GeneralSound.Play (sound_path);}

			OnQuestion (question);

			if (Course.cheat_mode)
				question.Cheat();

			question_start_time = getTimer();
		}
		public function OnQuestion (question:Question = null):void
		{
			if (function_on_question != null){
				var number:String = "";
				if (question) number = question.question_number;
				function_on_question (question, number);}
		}
		public function ShowSubmitButton ():void
		{
			clip.alpha = 0;
			clip.visible = false;
			clip.buttonMode = false;
			clip.removeEventListener(MouseEvent.CLICK, SubmitAssessment2);
			
			////
			
			
			addChild(clip_1);
			clip_1.alpha = 1;
			clip_1.visible = true;
			clip_1.x = 307;
			clip_1.y = 350;
			clip_1.buttonMode = true;
			clip_1.addEventListener (MouseEvent.CLICK, SubmitAssessment);
		}
		
		public function SubmitAssessment (evt:MouseEvent):void
		{
			evt.currentTarget.visible = false;
			evt.currentTarget.alpha = 0.7;
			evt.currentTarget.buttonMode = false;
			evt.currentTarget.removeEventListener (MouseEvent.CLICK, SubmitAssessment2);
			addChild(popup);
			//ShowActivityResponse(is_corrects);
			popup.x = 175;
			popup.y = 105;
			popup.goNext.addEventListener(MouseEvent.CLICK, navigateNext_1);			
			if(is_corrects){				
				//popup._txt.text = "Correct. "+correctFeedbackArr[Number(QuestionClassCount.globalQuestionCnt)-1];
				popup._txt.text = "Correct. ";
			}else{
				popup._txt.text = "Incorrect. ";
				//popup._txt.text = "Incorrect. "+correctFeedbackArr[Number(QuestionClassCount.globalQuestionCnt)-1];
			}
		}
		
		function navigateNext_1(evt:MouseEvent):void{
			OnQuestion();
			QuestionAnswered();
			HideSubmitButton();
			RemoveQuestion();
			ShowAnswers();
			popup.goNext.removeEventListener(MouseEvent.CLICK, navigateNext_1);
			removeChild(popup);
		}
		
		
		
		
		public function ShowSubmitButtonDummy():void
		{
			addChild(clip);
			clip.alpha = 1;
			clip.visible = true;
			clip.x = 307;
			clip.y = 350;
			clip.buttonMode = true;
			clip.addEventListener (MouseEvent.CLICK, SubmitAssessment2);
		}
		
		function SubmitAssessment2(evt:Event):void{				
				evt.currentTarget.alpha = 0.7;
				evt.currentTarget.buttonMode = false;
				evt.currentTarget.removeEventListener (MouseEvent.CLICK, SubmitAssessment2);
				addChild(popup);
				ShowActivityResponse(is_corrects);
				popup.x = 175;
				popup.y = 105;
				popup.goNext.addEventListener(MouseEvent.CLICK, navigateNext);
				if(is_corrects){
					//popup._txt.text = "Correct. "+correctFeedbackArr[Number(QuestionClassCount.globalQuestionCnt)-1];
					popup._txt.text = "Correct. ";
				}else{
					popup._txt.text = "Incorrect. ";
					//popup._txt.text = "Incorrect. "+correctFeedbackArr[Number(QuestionClassCount.globalQuestionCnt)-1];
				}
		}
		
		function navigateNext(evt:MouseEvent):void{
			QuestionAnswered();
			if (current_question_index < questions.length-1){
				RemoveQuestion();
				HideActivityResponse();
				current_question_index++;
				DisplayCurrentQuestion();
				}

			UpdateNavigation();
			popup.goNext.removeEventListener(MouseEvent.CLICK, navigateNext);
			removeChild(popup);
		}
		

		// Results functions ////////////////////////////////////////////////

		public function ShowAnswers ():void
		{
			if (Course.button_back){
				if (Course.button_events)
					Course.button_back.removeEventListener(MouseEvent.CLICK, NavigateBack);
				Course.ShowButton (Course.button_back, false);}

			if (Course.button_forward){
				if (Course.button_events)
					Course.button_forward.removeEventListener(MouseEvent.CLICK, NavigateForward);
				Course.ShowButton (Course.button_forward, false);}

			if (Course.button_cheat){
				if (Course.button_events)
					Course.button_cheat.removeEventListener(MouseEvent.CLICK, CheatQuestion);
				Course.ShowButton (Course.button_cheat, false);}

			results_sprite = new Sprite;
			this.addChild (results_sprite);

			var title_sprite:Sprite = GeneralText.MakeText ("Summary", Question.title_size, Question.title_color, 0, 0);
			results_sprite.addChild (title_sprite);

			var ypos_top:int = title_sprite.height + Question.title_gap;
			var ypos:int = ypos_top;
			var xpos:int = 0;
			var column_width:int = 0;
			for (var i:int=0; i<questions.length; i++)
			{
				var question:Question = questions[i];
				var is_correct:Boolean = question.IsCorrect();
				if (is_correct) number_correct++;

				var answer_sprite:Sprite = question.GetResultSprite();
				answer_sprite.x = xpos;
				answer_sprite.y = ypos;
				column_width = Math.max (column_width, answer_sprite.width);
				results_sprite.addChild (answer_sprite);
				ypos += answer_sprite.height + Question.result_gap;

				if (ypos > Course.page_height){
					xpos += column_width + 15;
					ypos = ypos_top;}
			}
			xpos += column_width + 15;

			var normalized_score:Number = Number(number_correct) / Number(question_count);
			var percentage_score:Number = Math.floor (normalized_score * 100)
			var passed:Boolean = Boolean (normalized_score >= pass_threshold);

			var message_text:String;
			if (passed){
				
				try {
							MovieClip(root).setCourseToComplete();
						}
					catch(e:Error) {
						//trace(e);
						}
				
				
				message_text = message_text_pass;
				
				
				if (scorm_mode){
					Scorm.AssessmentPassed (page_xml, percentage_score);}
				assessment_complete = true;
				if (function_success != null) function_success();
				else Course.UpdateNavigation();}
			else {
				var pass_string:String = String (int (pass_threshold * 100)) + "%";
				message_text = message_text_fail.replace ("$correct", number_correct).replace ("$total", question_count).replace ("$pass", pass_string);
				if (scorm_mode){
					Scorm.AssessmentFailed (page_xml, percentage_score);}}

			var message_x:int = xpos + int ((Course.page_width - 340) / 2);
			if (message_xpos) message_x = message_xpos;
			var message_y:int = 50;
			if (message_ypos) message_y = message_ypos;
			var message_sprite:Sprite = GeneralText.MakeText (message_text, message_size, message_color, message_x, message_y, 200);
			results_sprite.addChild (message_sprite);
			message_y += message_sprite.height + 70;

			if (!passed){
				if (buttons_xpos) message_x = buttons_xpos;
				if (buttons_ypos) message_y = buttons_ypos;
				if (button_retry_class) button_retry_sprite = MakeSymbolButton (button_retry_class, message_x, message_y);
				else button_retry_sprite = Course.MakeButton ("Retry the questions", message_x, message_y, message_button_width);
				message_y += button_gap;
				if (function_redo_stage != null){
					if (button_redo_stage_class) button_redo_stage_sprite = MakeSymbolButton (button_redo_stage_class, message_x, message_y);
					else button_redo_stage_sprite = Course.MakeButton ("Redo this stage", message_x, message_y, message_button_width);
					button_redo_stage_sprite.addEventListener (MouseEvent.CLICK, function_redo_stage);
					results_sprite.addChild (button_redo_stage_sprite);
					message_y += button_gap;}
				if (function_main_menu != null){
					if (button_main_menu_class) button_main_menu_sprite = MakeSymbolButton (button_main_menu_class, message_x, message_y);
					else button_main_menu_sprite = Course.MakeButton ("Back to main", message_x, message_y, message_button_width);
					button_main_menu_sprite.addEventListener (MouseEvent.CLICK, function_main_menu);
					results_sprite.addChild (button_main_menu_sprite);}
				button_retry_sprite.addEventListener (MouseEvent.CLICK, RetryQuestions);
				results_sprite.addChild (button_retry_sprite);}
		}
		public function MakeSymbolButton (_class:Class, xpos:int=0, ypos:int=0) : Sprite
		{
			var button:Sprite = new _class;
			button.x = xpos; button.y = ypos;
			button.buttonMode = true;
			return button;
		}
		public function RetryQuestions(e:Event):void
		{
			
			RemoveResults ();
			current_question_index = 0;
			for (var i:int=questions.length-1; i>=0; i--){
				var question:Question = questions[i];
				if (question.IsCorrect()){
					questions.splice (i,1);
					reserve_matrix.splice (i,1);}
				else question.Reset();}

			// substitute reserve questions
			for (i=0; i<questions.length; i++){
				question = questions[i];
				var question_number:String = question.question_number;
				var matrix_array:Array = reserve_matrix[i];
				if (matrix_array.length){
					matrix_array.push (question);
					question = matrix_array.shift();
					question.question_number = question_number;
					questions[i] = question;}
				else { // general reserves
					reserve_questions.push (question);
					question = reserve_questions.shift();
					question.question_number = question_number;
					questions[i] = question;}}

			AskQuestions ();
			
			
			/////
			//////////////
			trace("coming here");
			ShowSubmitButtonDummy();
			//addChild(clip);
				clip.alpha = 0.7;
				clip.x = 307;
				clip.y = 350;
				clip.buttonMode = false;
				clip.removeEventListener(MouseEvent.CLICK, SubmitAssessment2);
			////////////////
		}

		// Clean up functions ///////////////////////////////
		
		public function RemoveQuestion():void
		{
			if (question_sprite){
				this.removeChild (question_sprite);
				question_sprite = null;
				GeneralSound.Stop();}
		}
		public function RemoveResults():void
		{
			if (results_sprite){
				this.removeChild (results_sprite);
				results_sprite = null;}
		}
		
		public function QuestionAnswered ():void
		{
			if (scorm_mode){
				
				if (current_question_index < questions.length){
					var question:Question = questions[current_question_index];
					var is_correct:Boolean = question.IsCorrect();
					var correct_response:String = question.CorrectResponse();
					var student_response:String = question.StudentResponse();
					var question_xml:XML = question.question_xml;
					var latency:String = GetQuestionTime();
					Scorm.QuestionAnswered (question_xml, correct_response, student_response, latency, is_correct);}}
				/////////////////////////

		}
		public function GetQuestionTime ():String
		{
			var millisecs:Number = getTimer() - question_start_time;
			var hours:Number = Math.floor (millisecs / 3600000);
			millisecs -= hours * 3600000;
			var minutes:Number = Math.floor (millisecs / 60000);
			millisecs -= minutes * 60000;
			var seconds:Number = millisecs / 1000;
			var latency:String = (hours > 9) ? String (hours) : ("0" + hours);
			latency += ":" + ((minutes > 9) ? String (minutes) : ("0" + minutes));
			latency += ":" + ((seconds > 9) ? seconds.toFixed(2) : ("0" + seconds.toFixed(2)));
			return latency;
		}
		// Navigation functions //////////////////////////////////

		public override function NavigateForward (event:Event):Boolean
		{
			if (assessment_complete){
				return false;}

			QuestionAnswered();
			if (current_question_index < questions.length-1){
				RemoveQuestion();
				HideActivityResponse();
				current_question_index++;
				DisplayCurrentQuestion();}

			UpdateNavigation();
			return true;
		}
		public override function NavigateBack (event:Event):Boolean
		{
			if (assessment_complete){
				return false;}

			if (current_question_index > 0){
				RemoveQuestion();
				HideActivityResponse();
				current_question_index--;
				DisplayCurrentQuestion();}

			UpdateNavigation();
			return true;
		}
		public function UpdateNavigation():void
		{
			if (activity_mode){
				
				Course.ShowButton (Course.button_forward, false);
				Course.ShowButton (Course.button_back, false);
				//HideSubmitButton();
				}
			else {
				if (questions.length == 1){
					
					Course.ShowButton (Course.button_forward, false);
					Course.ShowButton (Course.button_back, false);}

				else if (current_question_index == 0){
					Course.ShowButton (Course.button_forward, true);
					Course.ShowButton (Course.button_back, false);}

				else if (current_question_index == questions.length - 1){
					Course.ShowButton (Course.button_forward, false);
					Course.ShowButton (Course.button_back, true);
					}
				else {
					Course.ShowButton (Course.button_forward, true);
					Course.ShowButton (Course.button_back, true);}

				//HideSubmitButton();
				if (current_question_index == questions.length - 1) 
					ShowSubmitButton();
				}
			
			Course.ShowButton (Course.button_cheat, true);
		}
		public function HideSubmitButton():void
		{
			if (button_submit_sprite) this.removeChild (button_submit_sprite);
			button_submit_sprite = null;
		}
		
		// Other stuff //////////////////////////

		public override function OnEnterFrame (event:Event):void
		{
			for (var j:int=0; j<questions.length; j++){
				var question:Question = questions[j];
				
				question.OnEnterFrame (event);}
		}
		public function CheatQuestion (event:Event = null):void
		{
			var question:Question = questions[current_question_index];
			question.Cheat();
		}
		public function ActivityHandler (question:Question, option:Option):void
		{
			if (activity_mode){
				is_corrects = question.IsCorrect();
				if (current_question_index != questions.length - 1){ 
					ShowSubmitButtonDummy();
					//trace("coming here == 11111");
				}else{
					ShowSubmitButton();
					
					//trace("coming here");
				}
				//ShowActivityResponse (is_correct);
				//trace("coming here" + option + " is_correct");
				if (Course.button_forward){
					if (is_corrects){ 
						//Course.ShowButton (Course.button_forward, true);
						if (current_question_index == questions.length - 1){
							if (scorm_mode){
								Scorm.AssessmentPassed (page_xml, 100);
								}
							if (function_success != null){
								//function_success();
								}}}
					else { // wrong
					
						Course.ShowButton (Course.button_forward, false);}}
			}
		}
		public function ShowActivityResponse (is_corrects:Boolean):void
		{
			
			HideActivityResponse();
			activity_sprite = new Sprite;
			var correct_count:int = (questions.length > 1 && current_question_index == questions.length - 1) ? 1 : 0;
			var text:String = is_corrects ? activity_correct[correct_count] : activity_wrong[activity_wrong_count];
			if (!is_corrects && activity_wrong_count < activity_wrong.length - 1) activity_wrong_count++;
			var text_sprite:Sprite = GeneralText.MakeText (text, activity_size, activity_color, 0, 0, Course.page_width - 100);
			activity_sprite.addChild (text_sprite);
			activity_sprite.x = 5;
			var ypos:int = question_sprite.height + 10;
			activity_sprite.y = ypos + (Course.page_height - activity_sprite.height - ypos) / 2;
			if (activity_xpos) activity_sprite.x = activity_xpos;
			if (activity_ypos) activity_sprite.y = activity_ypos;
			this.addChild (activity_sprite);
			HideActivityResponse();
		}
		public function HideActivityResponse ():void
		{
			if (activity_sprite){
				this.removeChild(activity_sprite);
				activity_sprite = null;
			}
		}
	}
}
