package source.courses
{
	import flash.display.*;
	import source.general.*;

	public class ElementSound extends Element
	{
		public var url_array:Array = [];
		public var function_finish:Function;

		public override function Setup (_xml:XML, _slide:CourseSlide):void
		{
			super.Setup (_xml, _slide);

			var urls:String = _xml.@url;
			var array:Array = urls.split(',');
			for each (var url:String in array){
				var trim:String = GeneralString.Trim (url);
				var path:String = Course.ResolveMediaPath ("sounds", trim);
				url_array.push (path);}

			is_playable = true;
			allow_timing = true;
		}
		public override function DoLayout (layout_width:int, layout_height:int, fix_width:Boolean=false, fix_height:Boolean=false):Boolean
		{
			if (has_played) return false;
			var volume:Number = Number (element_xml.@volume) / 100.0;
			GeneralSound.Play (url_array[0], volume, function_finish);
			has_played = true;
			return false;
		}
		public override function SetEnabled (state:Boolean):void
		{
			super.SetEnabled (state);
			if (!state) GeneralSound.Stop();
		}
		public override function DocumentElement ():String
		{
			return DocumentComment ("[ Play sound: " + url_array[0] + " ]");
		}
	}
}
