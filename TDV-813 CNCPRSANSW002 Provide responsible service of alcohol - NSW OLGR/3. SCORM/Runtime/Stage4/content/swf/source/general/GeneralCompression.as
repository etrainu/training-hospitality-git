﻿package source.general
{
	public class GeneralCompression
	{
		import flash.events.*;
		import flash.utils.*; 

		public static function Unzip (data:ByteArray):ByteArray
		{
			var offset:int = 30;  
			data.endian = Endian.LITTLE_ENDIAN;
			data.position = 26;  // file name length 
			offset += data.readShort(); 
			data.position = 28;    // extra field length 
			offset += data.readShort(); 
			data.position = offset;
			var buffer:ByteArray = new ByteArray;
			data.readBytes (buffer);
			buffer.inflate(); 
			return buffer;
		}
	}
}