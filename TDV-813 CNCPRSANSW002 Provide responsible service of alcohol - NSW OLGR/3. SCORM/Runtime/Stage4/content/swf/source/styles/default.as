﻿// AutoCourse Style Options ///////////////////////////////////////////////////
// default.as contains all the available options with default values
// do not change this file, save any changes to a different filename

// Change the default font and embedding mode
// All text is HTML so can use <b> for bold or <i> for italic
// Set a stylesheet for HTML tags

GeneralText.embed_fonts = false;
GeneralText.default_font = "Arial";
GeneralText.default_bold = false;
GeneralText.SetStyle ("")

// Set the position and size of the page

Course.page_x = 10;
Course.page_y = 10;
Course.page_width = 500;
Course.page_height = 400;

// Appearance of default buttons

Course.button_text_color = 0x303030;
Course.button_text_size = 15;
Course.button_background_color = 0x808080;
Course.button_background_alpha = 0.1;
Course.button_disabled_alpha = 0.3;

// The following section covers assessment options ////////////////////////////

// Appearance of the question pages.

Question.title_size = 24;
Question.title_color = 0x002040;
Question.title_gap = 20;
Question.text_size = 13;
Question.text_color = 0x000000;
Question.text_gap = 20;
Question.help_color = 0x707070;
Question.help_size = 11;
Option.text_color = 0x303030;
Option.text_size = 12;
Option.text_gap = 10;

// Changing these will override the width of question text or
// option text or draw the options at a different position

Question.options_xpos = 0;
Question.options_ypos = 0;
Question.options_width = 0;
Question.question_width = 0;

// Appearance of the result sumary page.
// If xpos and ypos are zero the buttons and messages will be auto-placed in reasonable positions

Question.result_size = 14;
Question.result_color = 0x000000;
Question.result_gap = 10;
Assessment.submit_xpos = 0;
Assessment.submit_ypos = 0;
Assessment.message_xpos = 0;
Assessment.message_ypos = 0;
Assessment.message_size = 16;
Assessment.message_color = 0x303030;
Assessment.message_text_pass = "Well done!<br><br>You have passed";
Assessment.message_text_fail = "You scored $correct out of $total<br><br>$pass is required to pass<br><br>Please try again";
Assessment.buttons_xpos = 0;
Assessment.buttons_ypos = 0;
Assessment.button_gap = 34;

// Appearance of drag and drop boxes

Option.border_color = 0x000000;
Option.border_alpha = 0.2;
Option.background_color = 0x000000;
Option.background_alpha = 0.02;
Option.horz_margin = 6;
Option.vert_margin = 3;
Option.corner_radius = 8;
Option.box_gap = 6;
OptionDropWindow.min_height = 68;

// Size of drag and drop images

OptionDragImage.image_width = 90;
OptionDragImage.image_height = 90;
OptionMatchImage.image_width = 160;
OptionMatchImage.image_height = 90;

// Assessment template has default scripted ticks and crosses
// Change their color and they will anti-alias to any background

ResultCross.color = 0x900000;
ResultTick.color = 0x006000;
OptionTick.color = 0x000080;

// Replace the scripted defaults with your own ticks, crosses, checks, and unchecks
// Just export the symbol for actionscript and assign the class name here

Question.result_tick_class = ResultTick;
Question.result_cross_class = ResultCross;
OptionMultipleChoice.option_checked_class = OptionTick;
OptionMultipleChoice.option_unchecked_class = OptionSquare;

// Replace the scripted buttons with your own buttons
// Just export the symbol for actionscript and assign the class name here

Assessment.button_submit_class = null;
Assessment.button_retry_class = null;
Assessment.button_redo_stage_class = null;
Assessment.button_main_menu_class = null;

// Activity mode turns off the submit button and results page
// Appearance and text of the message after each question

Assessment.activity_xpos = 0;
Assessment.activity_ypos = 0;
Assessment.activity_size = 14;
Assessment.activity_color = 0x606060;
Assessment.activity_wrong = ["Wrong answer ... try again", "Still wrong ... keep trying"];
Assessment.activity_correct = ["Correct ... well done!", "Correct again!<br><br>Congratulations, you have passed"];

// Disable scorm if you are sending scorm complete message yourself

Scorm.enable = true;

// The following section covers slide options /////////////////////////////////

ElementHeading.heading_type_array = ["large","medium","small"];
ElementHeading.text_size_array = [25,18,14];
ElementHeading.text_color_array = [0x909090,0x707070,0x606060];
ElementHeading.text_bold_array = [true,true,true];
ElementHeading.margin_top_array = [20,16,14];
ElementHeading.margin_bottom_array = [6,6,6];

// paragraph
ElementParagraph.text_size = 14;
ElementParagraph.text_color = 0x000000;
ElementParagraph.margin_top = 6;
ElementParagraph.margin_bottom = 6;

// list item
ElementListItem.text_size = 14;
ElementListItem.text_color = 0x000000;
ElementListItem.bullet_indent = 20;
ElementListItem.margin_top = 4;
ElementListItem.margin_bottom = 4;

// image
ElementImage.margin_top = 0;
ElementImage.margin_bottom = 0;











