package Utils.Containers 
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	/**
	 * ...
	 * @author Amir Zahedi
	 */
	public class ContainerWrapper 
	{
		
		public function ContainerWrapper() 
		{
			
		}
		public static function wrap(sourceContainer:Sprite):Sprite {
			var destinationContainer:Sprite = new Sprite();
			var object:DisplayObject;
			while (sourceContainer.numChildren > 0){
				object = sourceContainer.getChildAt(0);
				sourceContainer.removeChild(object);
				destinationContainer.addChild(object);
			}
			return destinationContainer
		}
	}

}