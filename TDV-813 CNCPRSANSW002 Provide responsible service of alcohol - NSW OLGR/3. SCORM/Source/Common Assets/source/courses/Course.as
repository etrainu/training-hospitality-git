﻿package source.courses
{
	import flash.display.*;
	import flash.events.*;
	import flash.utils.*;
	import source.custom.*;
	import source.general.*;

	public class Course
	{
		public static var editor:*;
		public static var canvas:Sprite;
		public static var page_x:int = 0;
		public static var page_y:int = 0;
		public static var page_width:int = 500;
		public static var page_height:int = 400;
		public static var button_back:*;
		public static var button_forward:*;
		public static var button_cheat:*;
		public static var button_events:Boolean = true;
		public static var cheat_mode:Boolean = false;
		public static var current_page:CoursePage;
		public static var media_folder:String = "media";
		public static var title_function:Function;
		public static var button_text_color:uint = 0x303030;
		public static var button_text_size:int = 15;
		public static var button_background_color:uint = 0x808080;
		public static var button_background_alpha:Number = 0.1;
		public static var button_disabled_alpha:Number = 0.3;
		public static var course_xml_list:XMLList;
		public static var course_xml:XML;
		public static var config_xml:XML;
		public static var course_id:String;

		public static function set xml (xml:XML):void
		{
			course_xml = EvolveXML (xml);
			CheckResourceLocation ();
			Scorm.CourseStart (xml);
			ConnectListeners ();
		}
		public static function LoadSameNameXML ():void
		{
			var course_url:String = canvas.stage.loaderInfo.url.replace (".swf",".xml");
			GeneralLoader.LoadURL (course_url, CourseLoaded);
		}
		public static function LoadSameNameZip ():void
		{
			var course_url:String = canvas.stage.loaderInfo.url.replace (".swf",".zip");
			GeneralLoader.LoadStream (course_url, CourseLoadedUnzip);
		}
		public static function LoadConfig ():void
		{
			GeneralLoader.LoadURL ("config.xml", ConfigLoaded);
		}
		public static function ConfigLoaded (loader:CustomURLLoader):void
		{
			config_xml = XML(loader.data);
			CourseStyles.SetStyles (config_xml.styles);

			course_xml_list = config_xml.course;
			if (course_xml_list.length()){
				LoadCourseEntry (course_xml_list[0]);}
		}
		public static function LoadCourseID (_id:String):void
		{
			if (course_xml_list){
				var course_entry:XML = course_xml_list.(@id == _id)[0];
				if (course_entry) LoadCourseEntry (course_entry);}
		}
		public static function LoadCourseEntry (course_entry:XML):void
		{
			course_id = course_entry.@id;
			var course_url:String = course_entry.@url;
			Course.media_folder = String (course_entry.@media);
			GeneralLoader.LoadURL (course_url, CourseLoaded);
		}
		public static function CourseLoaded (loader:CustomURLLoader):void
		{
			Scorm.started = false;
			xml = XML (loader.data);
			Course.PlayCourse();
		}
		public static function CourseLoadedUnzip (stream:CustomStream):void
		{
			Scorm.started = false;
			var zip_data:ByteArray = new ByteArray;
			stream.readBytes (zip_data);
			xml = XML (GeneralCompression.Unzip (zip_data));
			Course.PlayCourse();
		}
		public static function ResolveMediaPath (subfolder:String, filename:String):String
		{
			if (editor)
				return editor.ResolveMediaPath (subfolder, filename);

			var path:String = "";
			if (media_folder) path = media_folder + "/";
			if (subfolder) path += subfolder + "/";
			path = (path + filename).replace (/\/+/g, "/");
			return path;
		}
		public static function PlayCourse ():void
		{
			PlayStage ();
		}
		public static function PlayStage (id:* = 0):void
		{
			var stage_xml:XML = null;
			if (id is int) stage_xml = course_xml.content.stage[id];
			else if (id is String) stage_xml = course_xml.content.stage.(@name == id)[0];
			if (stage_xml) PlayStageXML (stage_xml);
		}
		public static function PlayStageXML (stage_xml:XML):void
		{
			var slide_xml_list:XMLList = stage_xml.children();
			if (slide_xml_list.length()){
				PlayPageXML (slide_xml_list[0]);}
		}
		public static function PlaySlide (id:* = 0):void // both
		{
			var slide_xml:XML = null;
			if (id is int) slide_xml = course_xml.content.stage.children()[id];
			else if (id is String) slide_xml = course_xml.content.stage.children().(@name == id)[0];
			PlayPageXML (slide_xml);
		}
		public static function PlayAssessment (id:* = 0):void
		{
			var ass_xml:XML = null;
			if (id is int) ass_xml = course_xml.content.stage.assessment[id];
			else if (id is String) ass_xml = course_xml.content.stage.assessment.(@name == id)[0];
			PlayPageXML (ass_xml);
		}
		public static function PlayPageXML (slide_xml:XML):void
		{
			if (title_function != null) title_function (slide_xml);

			if (slide_xml.name() == "assessment") NewAssessment (slide_xml).AskQuestions();
			else {
				CourseSlide (SetCurrentPage (new CourseSlide (slide_xml))).ShowSlide ();
				UpdateNavigation();}
		}
		public static function PlayElementXML (element_xml:XML):void
		{
			//Slide (SetCurrentPage (new Slide (element_xml))).ShowElementXML (element_xml);
		}
		public static function PlayQuestionXML (question_xml:XML):void
		{
			Assessment (SetCurrentPage (new Assessment (question_xml.parent(), true))).JumpToQuestion (question_xml.@ord);
		}
		public static function NewAssessment (ass_xml:XML):Assessment
		{
			return Assessment (SetCurrentPage (new Assessment (ass_xml)));
		}
		public static function SetCurrentPage (page:CoursePage):CoursePage
		{
			ClosePage();
			canvas.addChild (page);
			current_page = page;
			page.x = page_x;
			page.y = page_y;
			return page;
		}
		public static function ClosePage ():void
		{
			if (current_page){
				current_page.UnloadAndStop();
				if (canvas.contains (current_page)){
					canvas.removeChild (current_page);}
				current_page = null;}

			GeneralSound.Stop();
			GeneralVideo.Stop();
		}
		public static function PlayNextPage (direction:int=1):void
		{
			var next_page_xml:XML = GetNextPageXML (direction);
			if (next_page_xml) PlayPageXML (next_page_xml);
		}
		public static function GetNextPageXML (direction:int=1):XML
		{
			if (current_page){
				var page_xml:XML = current_page.page_xml;
				while (page_xml){
					var page_index:int = page_xml.childIndex();
					var next_index:int = page_index + direction;
					if (next_index < 0){
						var prev_stage_xml:XML = GetNextStageXML (direction);
						if (prev_stage_xml){
							var prev_stage_page_list:XMLList = prev_stage_xml.children();
							var stage_page_count:int = prev_stage_page_list.length();
							if (stage_page_count){
								var prev_stage_page:XML = prev_stage_page_list[stage_page_count-1];
								return prev_stage_page;}}
						return null;}
					var stage_xml:XML = page_xml.parent();
					var pages:XMLList = stage_xml.children();
					var page_count:int = pages.length();
					if (next_index < page_count){
						var next_page_xml:XML = pages[next_index];
						if (next_page_xml.name() == "assessment"){
							if (Scorm.IsAssessmentPassed (next_page_xml)){
								page_xml = next_page_xml;
								continue;}}
						return next_page_xml;}
					else if (next_index == page_count){
						var next_stage_xml:XML = GetNextStageXML (direction);
						if (next_stage_xml){
							var next_stage_page_list:XMLList = next_stage_xml.children();
							if (next_stage_page_list.length()){
								var next_stage_page:XML = next_stage_page_list[0];
								return next_stage_page;}}}
					break;}}
			return null;
		}
		public static function GetNextStageXML (direction:int=1):XML
		{
			if (current_page){
				var page_xml:XML = current_page.page_xml;
				var stage_xml:XML = page_xml.parent();
				var stage_index:int = stage_xml.childIndex();
				var next_stage_index:int = stage_index + direction;
				if (next_stage_index < 0) return null;
				var content_xml:XML = stage_xml.parent();
				var stages:XMLList = content_xml.content.stage;
				var stage_count:int = stages.length();
				if (next_stage_index < stage_count){
					var next_stage_xml:XML = stages[next_stage_index];
					return next_stage_xml;}}
			return null;
		}
		public static function RestartStage (event:Event=null):void
		{
			if (current_page){
				var page_xml:XML = current_page.page_xml;
				var stage_xml:XML = page_xml.parent();
				PlayStageXML (stage_xml);}			
		}		
		public static function CheatQuestion (event:Event=null):void
		{
			if (GetAssessment()){
				GetAssessment().CheatQuestion();}
		}
		public static function ClickBack (event:Event=null):void
		{
			if (current_page){
				if (!current_page.NavigateBack (event)){
					PlayNextPage (-1);}}
		}
		public static function ClickForward (event:Event=null):void
		{
			//Here we need to send passing status
					try {
							MovieClip(root).bookMarkCount = 14;
							MovieClip(parent).bookMarkCount = 14;
						}
					catch(e:Error) {
						//trace(e);
						}
				///////////////////////////////////
			if (current_page){
				if (!current_page.NavigateForward (event)){
					//Here we need to send passing status
					try {
							MovieClip(root).bookMarkCount = 14;
							MovieClip(parent).bookMarkCount = 14;
						}
					catch(e:Error) {
						//trace(e);
						}
				///////////////////////////////////
					PlayNextPage (1);}}
		}
		public static function GetAssessment ():Assessment
		{
			if (current_page && current_page is Assessment)
				return Assessment (current_page);
			return null;
		}
		public static function ConnectListeners ():void
		{
			if (button_events){
				if (button_forward) Course.button_forward.addEventListener(MouseEvent.CLICK, ClickForward);
				if (button_back) Course.button_back.addEventListener(MouseEvent.CLICK, ClickBack);
				if (button_cheat) Course.button_cheat.addEventListener(MouseEvent.CLICK, CheatQuestion);}

			canvas.addEventListener (Event.ENTER_FRAME, OnEnterFrame);
		}
		public static function RemoveListeners ():void
		{
			if (button_events){
				if (button_forward) Course.button_forward.removeEventListener(MouseEvent.CLICK, ClickForward);
				if (button_back) Course.button_back.removeEventListener(MouseEvent.CLICK, ClickBack);
				if (button_cheat) Course.button_cheat.removeEventListener(MouseEvent.CLICK, CheatQuestion);}

			canvas.removeEventListener (Event.ENTER_FRAME, OnEnterFrame);
		}
		public static function CheckResourceLocation ():void
		{
			if (canvas.stage.loaderInfo.parameters.hasOwnProperty ("resourceLocation")){
				media_folder = canvas.stage.loaderInfo.parameters.resourceLocation + "/media";}
		}
		public static function OnEnterFrame (event:Event):void
		{
			if (current_page)
				current_page.OnEnterFrame (event);
		}
		public static function Redraw():void
		{
			if (current_page && current_page is CourseSlide)
				Object (current_page).LayoutAndDraw ()
		}
		public static function UpdateNavigation():void
		{
			var prev_xml:XML = GetNextPageXML (-1);
			var next_xml:XML = GetNextPageXML (1);
			ShowButton (Course.button_back, (prev_xml != null) && prev_xml);
			ShowButton (Course.button_forward, (next_xml != null));
		}
		public static function ShowButton (button:*, state:Boolean):void
		{
			if (button){
				button.visible = true;
				button.alpha = state ? 1 : button_disabled_alpha;
				button.mouseEnabled = state;
				button.enabled = state;}
		}
		public static function MakeButton (text:String, xpos:int=0, ypos:int=0, _width:int=0) : Sprite
		{
			return GeneralText.MakeButton (text, button_text_size, xpos, ypos, _width, button_text_color, button_background_color, button_background_alpha);
		}
		public static function BackToMenu (event:Event):Boolean
		{
			if (course_xml_list){
				var menu_id:String = course_xml_list[0].@id;
				if (course_id != menu_id){
					ClosePage();
					LoadCourseID (menu_id);
					return true;}}
			return false;
		}
		public static function PageIndex ():int
		{
			return 0;
		}
		public static function PageCount ():int
		{
			return 0;
		}
		public static function EvolveXML (cxml:XML):XML
		{
			// questions v0.3
			var ques_xml_list:XMLList = cxml.stage.assessment.question.(@type == "choice");
			for each (var ques_xml:XML in ques_xml_list){
				ques_xml.@type = "multiple choice";}
			ques_xml_list = cxml.stage.assessment.question.(@type == "match");
			for each (ques_xml in ques_xml_list){
				ques_xml.@type = "match text text";}
			// assessments v0.4
			var ass_xml_list:XMLList = cxml.stage.assessment;
			for each (var xml:XML in ass_xml_list){
				if (!xml.@mode.length()){
					xml.@mode = "scorm";}
				if (xml.sound.length()){
					delete xml.sound;}}
			// stages v0.7
			var stage_xml_list:XMLList = cxml.stage;
			if (stage_xml_list.length()){
				var content_xml:XML = <content />;
				cxml.appendChild (content_xml);
				for each (var stage_xml:XML in stage_xml_list){
					content_xml.appendChild (stage_xml);}
				delete cxml.stage;}
			// width v0.8
			if (int (cxml.info.page.@width) == 0)
				cxml.info.page.@width = 600;
			if (int (cxml.info.page.@height) == 0)
				cxml.info.page.@height = 600;
			if (String (cxml.info.content.@folder) != "" && String (cxml.info.media.@folder) == ""){
				cxml.info.media.@folder = cxml.info.content.@folder;
				delete cxml.info.content.@folder;}
			// elements 0.9
			var element_xml_list:XMLList = cxml.content.stage.slide.element;
			for each (xml in element_xml_list){
				var type:String = xml.@type;
				if (type == "heading"){ if (String(xml.@style) == "") xml.@style = xml.@size;}
				if (type == "paragraph"){ if (String(xml.@style) == "") xml.@style = "medium";}
				if (type == "label"){ xml.@type = "group";}
				if (type == "align" && String(xml.@align) == "padding"){ xml.@align = "gap";}
				if (type == "image"){ if (String(xml.@alpha) == "") xml.@alpha = "100";}}
			return cxml;
		}
	}
}
