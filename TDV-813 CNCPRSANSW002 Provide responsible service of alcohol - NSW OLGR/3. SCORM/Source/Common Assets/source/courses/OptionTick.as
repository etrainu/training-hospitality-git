package source.courses
{
	import flash.display.*;
	
	public class OptionTick extends Sprite
	{
		public static var color:uint = 0x000080;
		
		public function OptionTick ()
		{
			var s:Sprite = new Sprite;
			var g:Graphics = s.graphics;
			g.lineStyle (3, color, 1);
			g.moveTo (8,34);
			g.curveTo (18,17, 32,6);
			g.moveTo (12,34);
			g.curveTo (18,22, 32,6);
			g.lineStyle (2, color);
			g.moveTo (32,6);
			g.lineTo (36,3);
			g.lineStyle (3, color);
			g.moveTo (9,34);
			g.curveTo (7,30, 2,20);
			g.moveTo (12,34);
			g.curveTo (12,30, 4,20);
			s.scaleX = s.scaleY = 0.4;
			s.x = 5; s.y = 4;
			addChild (s);
		}
	}
}
