﻿package com.etrainu.loader 
{
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.ProgressEvent;
	import flash.net.URLRequest;
	import com.greensock.TweenLite;
	import com.greensock.easing.*;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.AsyncErrorEvent;
	import flash.events.IOErrorEvent
	import flash.events.SecurityErrorEvent;
	import flash.events.ErrorEvent;
	import flash.text.TextField;
	import flash.system.LoaderContext;
	import flash.system.Security;
	import flash.system.ApplicationDomain;
	/**
	 *
	 * ...
	 * @author ...
	 */
	public class LoaderDC extends MovieClip 
	{
		public var progressMC:MovieClip;
		private var req:URLRequest;
		private var loader:Loader = new Loader();
		public function LoaderDC() 
		{
			addEventListener(Event.ADDED_TO_STAGE, init, false, 0, true);
		}
		private function init(e:Event):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			stageResizeHandler();
			stage.addEventListener(Event.RESIZE, stageResizeHandler, false, 0, true);
			loader.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, onLoaderProgress, false, 0, true);
			
			
			loader.addEventListener(AsyncErrorEvent.ASYNC_ERROR, errorHandlerAsyncErrorEvent);
			loader.addEventListener(IOErrorEvent.IO_ERROR, errorHandlerIOErrorEvent);
			loader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, errorHandlerSecurityErrorEvent);
			loader.contentLoaderInfo.addEventListener(Event.INIT, initHandler);
			loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, infoIOErrorEvent);

			Security.loadPolicyFile("http://etrainu.lms.sydney.s3-ap-southeast-2.amazonaws.com/crossdomain.xml");
			var context:LoaderContext = new LoaderContext();
			context.checkPolicyFile = true;
			context.applicationDomain = ApplicationDomain.currentDomain;
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onLoaderComplete, false, 0, true);
			var domain:String = this.loaderInfo.loaderURL;
			if(domain){
				if(domain.indexOf("/")){
					domain = domain.slice(0, domain.lastIndexOf("/")+1);
				}else if(domain.indexOf("\\")){
					domain = domain.slice(0, domain.lastIndexOf("\\")+1);
				}
				req = new URLRequest(domain+"main.swf");
			}else{
				req = new URLRequest("./main.swf");
			}
			loader.load(req, context);
		}
		protected function stageResizeHandler(event:Event = null):void {
			progressMC.x = (stage.stageWidth - progressMC.width)*0.5;
			progressMC.y = (stage.stageHeight - progressMC.height)*0.5;
		}
		private function onLoaderProgress(e:ProgressEvent):void {
			var percent:uint = (e.bytesLoaded / e.bytesTotal) * 100;
			//logTxt.text += 'percent: '+percent+'\r';
			progressMC.gotoAndStop(percent);
			if (e.bytesLoaded == e.bytesTotal) {
				//logTxt.text += 'completed to: ' + percent + '\r';
				loader.contentLoaderInfo.removeEventListener(Event.COMPLETE, onLoaderComplete);
				onLoaderComplete(null);
			}
		}
		private function onLoaderComplete(e:Event):void {
			//logTxt.text += 'onLoaderComplete\r';
			
			TweenLite.to(progressMC, 1, {alpha:0, onComplete:addContent, ease:Expo.easeOut} );
		}
		private function addContent():void {
			removeChild(progressMC);
			progressMC = null;
			stage.removeEventListener(Event.RESIZE, stageResizeHandler);
			addChild(loader);
			TweenLite.from(loader, 1, {alpha:0, delay:0.3, ease:Expo.easeOut} );
		}
		
		private function initHandler( e:Event ):void {
			//logTxt.text += "logTxt\r";
		  trace( 'load init' );
		}
		private function errorHandlerErrorEvent( e:ErrorEvent ):void {
			//logTxt.text += 'errorHandlerErrorEvent ' + e.toString() +"\r";
		  trace( 'errorHandlerErrorEvent ' + e.toString() );
		}
		private function infoIOErrorEvent( e:IOErrorEvent ):void {
			//logTxt.text += 'infoIOErrorEvent ' + e.toString() +"\r";
		  trace( 'infoIOErrorEvent ' + e.toString() );
		}
		private function errorHandlerIOErrorEvent( e:IOErrorEvent ):void {
			//logTxt.text += 'errorHandlerIOErrorEvent ' + e.toString() +"\r";
		  trace( 'errorHandlerIOErrorEvent ' + e.toString() );
		}
		private function errorHandlerAsyncErrorEvent( e:AsyncErrorEvent ) :void {
			//logTxt.text += 'errorHandlerAsyncErrorEvent ' + e.toString() +"\r";
		  trace( 'errorHandlerAsyncErrorEvent ' + e.toString() );
		}
		private function errorHandlerSecurityErrorEvent( e:SecurityErrorEvent ):void {
			//logTxt.text += 'errorHandlerSecurityErrorEvent ' + e.toString() +"\r";
		  trace( 'errorHandlerSecurityErrorEvent ' + e.toString() );
		}

	}

}