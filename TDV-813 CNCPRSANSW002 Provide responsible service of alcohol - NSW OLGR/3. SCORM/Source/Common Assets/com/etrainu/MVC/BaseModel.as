﻿package com.etrainu.MVC
{
	import flash.events.ContextMenuEvent;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.ui.ContextMenu;
	import flash.ui.ContextMenuBuiltInItems;
	import flash.ui.ContextMenuItem;
	import flash.system.LoaderContext;
	import flash.system.Security;
	import flash.system.ApplicationDomain;
	/**
	 * ...
	 * @author Amir Zahedi
	 */
	public class BaseModel extends EventDispatcher implements IModel
	{
		public static const MODEL_ITEM_CHANGE:String = "modelItemChange";
		public static const SCO_DATA_LOADED:String = "scoDataLoaded";
		public static const APP_HALT:String = "appHalt";
		public static const APP_FREEZ:String = "appFreez";
		public static const APP_MELTDOWN:String = "appMeltdown";
		
		public var totalItems:uint;
		public var data:*;
		public var loader:URLLoader;
		public var currentScoData:XML = new XML();
		public var doteesContextMenu:ContextMenu;
		
		public var preventExecution:Boolean = false;
		protected var _currentIndexId:String = "";
		protected var _currentIndex:int = -1;
		protected var scoList:XMLList = new XMLList();
		protected  var evt:Event;
		public function BaseModel() {
			loader = new URLLoader();
			loader.addEventListener(Event.COMPLETE, dataLoaded, false, 0, true);
			doteesContextMenu = new ContextMenu();
		}
		public function load(req:URLRequest):void {
			loader.load(req);
		}
		public function saveSession(addProgress:Boolean = true):void {
			
		}
		protected function dataLoaded(event:Event):void {
			data = new XML(loader.data as String);
			data = bakeXmlData(data);
			scoList = data..SCO;
			//trace("scoList: \n"+scoList);
			totalItems = scoList.length();
			dispatchPackageInitialization();
		}
		protected function dispatchPackageInitialization():void {
			evt = new Event(SCO_DATA_LOADED, true);
			this.dispatchEvent(evt);
		}
		protected function bakeXmlData(rawData:XML):XML {
			var globalqueue:uint = 0;
			for (var i:uint = 0; i < rawData.SCOList.Chapter.length(); i++) {
				rawData.SCOList.Chapter[i].@id = i;
				for (var j:uint = 0; j < rawData.SCOList.Chapter[i]..SCO.length(); j++) {
					globalqueue += 1;
					rawData.SCOList.Chapter[i]..SCO[j].@folderPath = rawData.SCOList.Chapter[i].@folderPath;
					rawData.SCOList.Chapter[i]..SCO[j].@chapterId = i;
					rawData.SCOList.Chapter[i]..SCO[j].@queue = j+1;
					rawData.SCOList.Chapter[i]..SCO[j].@globalqueue = globalqueue;
					rawData.SCOList.Chapter[i]..SCO[j].@id = globalqueue+ ":" +rawData.SCOList.Chapter[i]..SCO[j].@folderPath + ":" + rawData.SCOList.Chapter[i]..SCO[j].@file;
				}
			}
			return rawData
		}
		protected function updateData():void {
			currentScoData = getSCODataById(currentIndexId);
			_currentIndex = Number(currentScoData.@globalqueue-1);
			this.dispatchEvent(new Event(BaseModel.MODEL_ITEM_CHANGE));
			saveSession();
		}
		protected function controllCurrentIndex():void {
			
			if (currentIndex >= totalItems) {
				currentIndex = 0;
			}else if(currentIndex < 0) {
				currentIndex = totalItems-1;
			}
		}
		
		
		
		public function getSCODataById(scoId:String):XML {
			var scoDataCell:XML;
			for (var i:int = 0; i < totalItems; i++) {
				if(scoId == scoList[i].@id){
					scoDataCell = scoList[i];
					break
				}
			}
			return scoDataCell
		}
		//Getters & Setters
		public function set currentIndexId(indexId:String):void {
			if(!preventExecution){
				_currentIndexId = indexId;
			}
		}
		public function get currentIndexId():String {
			return _currentIndexId;
		}
		
		public function set currentIndex(index:int):void {
			if(!preventExecution){
				trace("currentIndex before control: "+index);
				_currentIndex = index;
				controllCurrentIndex();
				trace("currentIndex after control: "+_currentIndex);
				trace("currentIndexId: "+scoList[_currentIndex].@id);
				//trace("scoList: "+scoList);
				currentIndexId = scoList[_currentIndex].@id;
				//updateData();
			}
		}
		public function get currentIndex():int {
			return _currentIndex;
		}
	}

}