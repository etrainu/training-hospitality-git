﻿package com.etrainu.ras_nsw.views.slideview.Services {
	
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import fl.controls.Button;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import Utils.*;
	
	public class PopUp extends MovieClip {
		// Constants:
		private const padding:uint = 10;
		// Public Properties:
		// Private Properties:
		private var _content:MovieClip;
		private var _okButton:Boolean = true;
		private var okButtonRemoved:Boolean = false;
		private var evt:Event;
		// UI Elements:
		public var closeBtn:MovieClip;
		public var okBtn:Button;
		public var PopBack:MovieClip;
		public var hasContent:Boolean;
		public var autoSize:Boolean;
		public var popParams:Object;
		
		private static var instance:PopUp = null;
		private static var allowInstantiation:Boolean = false;
		
		private var global:Global;
		// Initialization:
		public function PopUp() {
			configUI();
			global = Global.getInstance();
			autoSize = true;
			hasContent = false;
			popParams = { };
			//Add Event Listeners
			TooltipManager.setTip(closeBtn, "بستن", "Close");
			closeBtn.addEventListener(MouseEvent.CLICK, CloseClicked)
			okBtn.addEventListener(MouseEvent.CLICK, OkClicked);
			PopBack.addEventListener(MouseEvent.MOUSE_DOWN, startDragPop);
			PopBack.addEventListener(MouseEvent.MOUSE_UP, stopDragPop);
		}
		
		public static function getInstance() :PopUp {
			if ( PopUp.instance == null ) {
				PopUp.allowInstantiation = true;
				PopUp.instance = new PopUp();
				PopUp.allowInstantiation = false;
			}
			return PopUp.instance;
		}
		// Public Methods:
		public function get content():MovieClip {
			return _content;
		}
		public function set content(d:MovieClip):void {
			_content = d;
			resetContent();
		}
		
		public function get okButton():Boolean {
			return _okButton;
		}
		public function set okButton(o:Boolean):void {
			_okButton = o;
			refreshOkBtn();
		}
		public function setSize(w:Number, h:Number):void {
			PopBack.width = w;
			PopBack.height = h;
			closeBtn.x = w - closeBtn.width - padding;
			okBtn.y = h - okBtn.height - padding;
		}
		public function resizeFor(w:Number, h:Number):void {
			var ocuppiedWidth:Number;
			var ocuppiedHeight:Number;
			ocuppiedWidth = w + (2 * padding);
			ocuppiedHeight = h + (3 * padding);
			if (_okButton) {
				ocuppiedHeight += okBtn.height;
			}
			setSize(ocuppiedWidth, ocuppiedHeight);
		}
		// Protected Methods:
		// Private Methods:
		private function refreshOkBtn():void {
			if (!_okButton) {
				if(!okButtonRemoved){
					removeChild(okBtn);
				}
				okButtonRemoved = true;
			}else if (okButtonRemoved) {
				addChild(okBtn);
				okButtonRemoved = false;
			}
		}
		private function resetContent():void {
			_content.y = padding;
			_content.x = padding;
			if (!hasContent) {
				addChildAt(_content, 1);
				hasContent = true;
			}else {
				removeChildAt(1);
				addChildAt(_content, 1);
			}
			if (_content == null) {
				hasContent = false;
			}
			if (autoSize) {
				resizeFor(_content.width, _content.height);
			}
		}
		//Event Handlers
		public function CloseClicked(event:MouseEvent = null, immediate:Boolean = false):void {
			//global.root.enableContainer(immediate);
			//global.root.removeChild(this);
			evt = new Event(Event.CLOSE, popParams);
			this.dispatchEvent(evt);
		}
		public function OkClicked(event:MouseEvent = null, immediate:Boolean = true):void {
			evt = new Event(Event.COMPLETE, popParams);
			this.dispatchEvent(evt);
			content.finalize();
			CloseClicked(null, immediate);
		}
		private function startDragPop(event:MouseEvent):void {
			this.startDrag();
		}
		private function stopDragPop(event:MouseEvent):void {
			this.stopDrag();
		}
		protected function configUI():void { }
	}
}