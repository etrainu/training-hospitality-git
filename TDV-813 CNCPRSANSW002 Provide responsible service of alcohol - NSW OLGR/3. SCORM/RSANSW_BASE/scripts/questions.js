
/*-----------------------------------------------------------------------------------

	Standard debug info
	
-------------------------------------------------------------------------------------*/

DEBUG_SCRIPT_NAME="$Source$"
DEBUG_SCRIPT_DESCRIPTION="Client side Assessment object test verification\n"
											   + "Any student interaction is handled by this script file"
DEBUG_VERSION_NUMBER="$Revision$"
DEBUG_AUTHOR="Grant Focas"
DEBUG_WRITTEN="20 May 2003"
DEBUG_LAST_MODIFIED="27 October 2008"


addLoadEvent(setCorrectAnswerLayer);

/*-----------------------------------------------------------------------------------

	Called from a feedback page
	It checks the documents paragraph collection and sets
	the style.display property for each paragraph that has
	a class of "feedbackCorrect" or "feedbackIncorrect",
	to ensure the appropriate feedback is displayed
	
-------------------------------------------------------------------------------------*/
function setCorrectAnswerLayer() {
	if(document.getElementById('feedback')){
		// get document location and find the querystring value
		var  loc=document.location.toString();
		var qsValue= loc.substr(loc.indexOf("=") + 1);
		var correct=(qsValue==1);
		// retrieve the paragraphs collection
		collElements=document.getElementsByTagName("p");
		
		// iterate thru the array, and set appropriate values
		for (var x=0; x<collElements.length; x++) {
			var para=collElements[x];
			var paraName=para.className;
			
			if (paraName.indexOf('feedbackCorrect') > -1) {		
				if (correct==false) {
					para.style.display='none';
				}
			} else if (paraName.indexOf('feedbackIncorrect') > -1) {
				if (correct==true) {
					para.style.display='none';
				}
			}	
		}					   
	}
}

/*-----------------------------------------------------------------------------------

	Check a True/False answer (whose elements have id of elementsId) and verify if the answer was correct
	If correct, redirect to url with a querystring c=1 or c=0	
	url:				URL to redirect to
	elementId:		The id suffix of the elements to check
	correctIndex: Index of the answer that should be selected
-------------------------------------------------------------------------------------*/
function checkTFAnswer(url, elementId, correctIndex) {

	var  success=isTrueFalseCorrect(elementId, correctIndex)
	var qsValue= "?c=";
	qsValue+=(success == true)? "1" : "0";
	return openFeedbackWindow(url + qsValue);
}

/*-----------------------------------------------------------------------------------

	Check a multiple choice answer (whose elements have id of elementsId) and verify if the answer was correct
	If correct, redirect to url with a querystring c=1 or c=0	
	
-------------------------------------------------------------------------------------*/
function checkMCAnswer(url, elementId) {

	var  success=isOptionGroupCorrect(elementId)
	var qsValue= "?c=";
	qsValue+=(success == true)? "1" : "0";
	return openFeedbackWindow(url + qsValue);
}

/*-----------------------------------------------------------------------------------

	Check a Missing word answer (whose elements have id of elementsId) and verify if the answer was correct
	If correct, redirect to url with a querystring c=1 or c=0	
	
-------------------------------------------------------------------------------------*/
function checkMWAnswer(url, elementId, MW_count) {
	var  success=isMissingWordsGroupCorrect(elementId, MW_count);
	var qsValue= "?c=";
	qsValue+=(success == true)? "1" : "0";
	return openFeedbackWindow(url + qsValue);
}

/*-----------------------------------------------------------------------------------

	Opens a standard feedback window with the provided
	url
	
-------------------------------------------------------------------------------------*/
function openFeedbackWindow(url) {
	window.open(url,"","toolbars=1,scrollbars=1,resizable=1,width=400,height=400,menu=0, left=200, top=200")
	return false;
}

/*-----------------------------------------------------------------------------------

	iterate thru the option group. If an option is checked, and it also has the 'correct' attribute set to true
	 then we have a winner	
	
-------------------------------------------------------------------------------------*/
function isMissingWordsGroupCorrect(elementId, MW_count) {
	
	for (var x=1; x<=MW_count; x++) {
		var sParaAnswer=document.getElementById('mw_' + elementId + '_' + x)
		
		if ((document.getElementById('txt_' + elementId + '_' + x).value.toLowerCase()) == (sParaAnswer.innerHTML.toLowerCase())) {
			// correct
		} else {
			return false;
		}
	}
	return true
}

/*-----------------------------------------------------------------------------------

	iterate thru the option group. If an option is checked, and it also has the 'correct' attribute set to true
	 then we have a winner	
	
-------------------------------------------------------------------------------------*/
function isOptionGroupCorrect(elementId) {
	var  collOptionGroup=document.getElementsByName(elementId)
	var correctCount = 0;
	for (var x=0; x<collOptionGroup.length; x++) {
		if(collOptionGroup[x].checked ==true && getAttribute(collOptionGroup[x],"size")!='1') {
			return false; //checked but not correct
		}
		if(collOptionGroup[x].checked ==false && getAttribute(collOptionGroup[x],"size")=='1') {
			return false; //not checked but correct
		}
		//if you have got this far you have answered all the correct choices and none of the incorrect ones
	}
	return true;
}

/*-----------------------------------------------------------------------------------

	Check the radio buttons with the provided suffix, and 
	see if the correct one was checked. ID had 'inp_' prefixed
	to the provided suffix	
-------------------------------------------------------------------------------------*/
function isTrueFalseCorrect(elementId, correctIndex) {
	var  collRadioGroup=document.getElementsByName('inp_' + elementId)
	
	for (var x=0; x<collRadioGroup.length; x++) {
		if ((collRadioGroup[x].checked ==true) && (x == correctIndex)) {
			return true
		}
	}
	return false
}

/*-----------------------------------------------------------------------------------

	browser independent get attribute method
	
-------------------------------------------------------------------------------------*/
function getAttribute(elem,strAttName) {
	
	var strAttValue=null
	try {
		strAttValue=elem.getAttribute(strAttName)
		return strAttValue
	} catch(e) {
		return null
	}
}

function showDom(elem) {
	var x=0;
	var results="";
	
	for (var prop in elem) {
		x++;
		if (x > 5) {
			alert (results);
			results="";
			x=0;
		}
		results+=(prop + ': ' + elem[prop] + '\n');
	}
	alert (results);
}