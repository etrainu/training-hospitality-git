function addLoadEvent(func) {
  var oldonload = window.onload;
  if (typeof window.onload != 'function') {
    window.onload = func;
  } else {
    window.onload = function() {
      if (oldonload) {
        oldonload();
      }
      func();
    }
  }
}


addLoadEvent(InitGlobalNavigation);





function InitGlobalNavigation(){
     
        var uniqueId = getQuerystring('UniqueId');     
        
 if(uniqueId!=null){
   
         var ulEls = document.getElementsByTagName('ul');
       // alert(ulEls.length);
         
         for (var i = 0; i < ulEls.length; i++) 
          {       
                 
           
	        if (((ulEls[i].className != null) && (ulEls[i].className != "")))
	         {
                          var className =ulEls[i].className;                 
                          var idValue= ulEls[i].getAttribute('id');
                              if(idValue==uniqueId){                                 
                                 ulEls[i].style.display = "block";;
                                 }
                              else{
                               //  ulEls[i].style.display = "none";
                                 }
     
              }                    
           
            
         }//end for loop
}
}



function getQuerystring(key, default_)
{
 
  key = key.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regex = new RegExp("[\\?&]"+key+"=([^&#]*)");
  var qs = regex.exec(window.location.href);
  if(qs == null)
    return null;
  else
    return qs[1];
}

