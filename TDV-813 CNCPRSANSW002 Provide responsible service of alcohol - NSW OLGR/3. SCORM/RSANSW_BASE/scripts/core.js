//core.js  last modified August 14 2009

var popupLinkConfig = new Array;
popupLinkConfig["popup"] = new Array ( "", "width=620,height=500,scrollbars=yes,menubar=no,resizable=yes");
popupLinkConfig["glossary"] = new Array ( "help", "width=600,height=500,scrollbars=yes,menubar=no,resizable=yes");
popupLinkConfig["applets"] = new Array ( "help", "width=630,height=550,scrollbars=yes,menubar=no,resizable=yes");

function addLoadEvent(func) {
  var oldonload = window.onload;
  if (typeof window.onload != 'function') {
    window.onload = func;
  } else {
    window.onload = function() {
      if (oldonload) {
        oldonload();
      }
      func();
    }
  }
}

addLoadEvent(initPrintAndOrCloseLinks);	
addLoadEvent(initPopupLinks);	
addLoadEvent(initExternalLinks);
addLoadEvent(InitCustomFooterLinks);

function initPrintAndOrCloseLinks(){
	if(typeof document.getElementById != 'undefined'){
		if(document.getElementById('popup') || document.getElementById('feedback')){
				addPrintAndCloseLinks();					   
		} else if(document.getElementById('show')){
				addPrintLink();					   
		} 
	}
}

function initPopupLinks()
{
  if (!document.getElementsByTagName) return true;
  var pageLinks = document.getElementsByTagName("a");
  for (var i = 0; i < pageLinks.length; i++) 
  {
	if (((pageLinks[i].className != null) && 
         (pageLinks[i].className != "")) ||
        ((pageLinks[i].parentNode.className != null) && 
         (pageLinks[i].parentNode.className != "")))
    {
      var linkClass = " " + pageLinks[i].className + " ";
      if ((linkClass == "  ") && (pageLinks[i].parentNode.className != ""))
      {
        linkClass = " " + pageLinks[i].parentNode.className + " ";
      }
      for (var theKey in popupLinkConfig) 
      {
	   if (linkClass.indexOf(" " + theKey + " ") > -1)
        {
          if ((pageLinks[i].target == "") || (pageLinks[i].target == null))
          {
            pageLinks[i].target = (popupLinkConfig[theKey][0] != "") ? popupLinkConfig[theKey][0] : theKey;
          }
            
		  pageLinks[i].settings = popupLinkConfig[theKey][1];
          pageLinks[i].onclick = popUp;
        }
      }
    }
  }
  return true;
}

function initExternalLinks(){
    var links = document.getElementsByTagName("A");
    for (var i=0;i<links.length;i++){
		var currentClass = links[i].className;
		if(currentClass.indexOf('external')>-1 || currentClass.indexOf('dictionary')>-1 || currentClass.indexOf('wikipedia')>-1 || currentClass.indexOf('document')>-1){
		    links[i].setAttribute('target', '_blank');
		}
	}
}

function InitCustomFooterLinks(){
  var footerSpan = document.getElementById('customFooter');
  if(footerSpan!=null){
  var links =footerSpan.getElementsByTagName("A");
  
    for (var i=0;i<links.length;i++){	
	   links[i].setAttribute('target', '_blank');
	}
	
	}//end if;
}
function popUp()
{
  newWin = window.open(this.href, this.target, this.settings);
  newWin.focus();
  return false;
} 

function addPrintAndCloseLinks() {
	if (!document.getElementById || !document.createTextNode) {return;} // Check for DOM support
	if (!document.getElementById("nav")) {return;} // Check that the nav element actually exists
	if (!window.print) {return;} // Check that the browser supports window.print
	if (document.getElementById('standard')) {return;} // Check that the page is not a standard page
	var oTarget = document.getElementById("nav");
	var oList = document.createElement('ul'); //add the list
	oList.setAttribute("id","navList");
	var oListItem = document.createElement('li');
	var oLink = document.createElement('a');
	oLink.id = 'printLink'; // Give the link an id to allow styling
	oLink.href = '#'; // Make the link focusable for keyboard users
	oLink.appendChild(document.createTextNode("Print"));
	oLink.onclick = function() {window.print(); return false;} // Return false prevents the browser from following the link and jumping to the top of the page after printing
	var oListItem2 = document.createElement('li');
	var oLink2 = document.createElement('a');
	oLink2.id = 'closeLink'; // Give the link an id to allow styling
	oLink2.href = '#'; // Make the link focusable for keyboard users
	oLink2.appendChild(document.createTextNode("Close"));
	oLink2.onclick = function() {window.close(); return false;} // Return false prevents the browser from following the link
	oListItem2.appendChild(oLink2);
	
	
	oList.appendChild(oListItem);
	oList.appendChild(oListItem2);
	oListItem.appendChild(oLink);
	oListItem2.appendChild(oLink2)
	oTarget.appendChild(oList);
}
function addPrintLink() {
	if (!document.getElementById || !document.createTextNode) {return;} // Check for DOM support
	if (!document.getElementById("nav")) {return;} // Check that the nav element actually exists
	if (!window.print) {return;} // Check that the browser supports window.print
	if (document.getElementById('standard')) {return;} // Check that the page is not a standard page
	var oTarget = document.getElementById("nav");
	var oList = document.createElement('ul'); //add the list
	oList.setAttribute("id","navList");
	var oListItem = document.createElement('li');
	var oLink = document.createElement('a');
	oLink.id = 'printLink'; // Give the link an id to allow styling
	oLink.href = '#'; // Make the link focusable for keyboard users
	oLink.appendChild(document.createTextNode("Print"));
	oLink.onclick = function() {window.print(); return false;} // Return false prevents the browser from following the link and jumping to the top of the page after printing
	
	
	oList.appendChild(oListItem);
	oListItem.appendChild(oLink);
	oTarget.appendChild(oList);
}
//________handleEnter and print________________________________________________________________________________________________________________
var ACTION_PRINT='print';
var ACTION_CLOSE='close';
var ACTION_MC = 'multiple_choice';
var ACTION_TF = 'true_false';
var ACTION_MW = 'missing_word';
var ACTION_SA = 'short_answer';

function handleEnter(action,ev,file,identifier,truefalse, count) {
	var ENTER_KEY=13;
	var keyCode = ev.keyCode ? ev.keyCode : ev.which ? ev.which : ev.charCode;
	if (keyCode == ENTER_KEY) {
		switch(action){
			case ACTION_PRINT:
				printPage();
				return false;
			case ACTION_CLOSE:
				window.close();
				return false;
			case ACTION_MC:
				return checkMCAnswer(file, identifier);
				return false;
			case ACTION_TF:
				return checkTFAnswer(file, identifier, truefalse);
				return false;
			case ACTION_MW:
				return checkMWAnswer(file, identifier, count);
				return false;
			case ACTION_SA:
				return openFeedbackWindow(file, identifier);
				return false;
			default:
				return false;			
		}
	}
}

function printPage(){
	window.print?
		window.print()
		: alert('Sorry, your browser does not support this feature. Please choose print from the file menu.');
}


//_________showTranscript for audio options____________________________________________________________________________________________________


function showTranscript(){
	document.getElementById('transcript').style.visibility = 'visible';
	document.getElementById('audioShow').style.display = 'none';
	return false;
 }


//_________setting value for scroll attribute of body tag______________________________________________________________________________________

//sets scroll to no for IE/Windows and yes for everything else. This avoids double scroll bars (the style3000 CSS uses divs which have scrolling)

function setScrollValue(){
	if(navigator.userAgent.indexOf("MSIE") != -1 && navigator.platform.indexOf("Mac") == -1){
		document.getElementsByTagName("body")[0].setAttribute("scroll","no");
	}else{
		document.getElementsByTagName("body")[0].setAttribute("scroll","yes");
	}
}

/**
 * SWFObject v1.4.4: Flash Player detection and embed - http://blog.deconcept.com/swfobject/
 *
 * SWFObject is (c) 2006 Geoff Stearns and is released under the MIT License:
 * http://www.opensource.org/licenses/mit-license.php
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * **SWFObject is the SWF embed script formerly known as FlashObject. The name was changed for
 *   legal reasons.
 */
if(typeof deconcept=="undefined"){var deconcept=new Object();}
if(typeof deconcept.util=="undefined"){deconcept.util=new Object();}
if(typeof deconcept.SWFObjectUtil=="undefined"){deconcept.SWFObjectUtil=new Object();}
deconcept.SWFObject=function(_1,id,w,h,_5,c,_7,_8,_9,_a,_b){if(!document.getElementById){return;}
this.DETECT_KEY=_b?_b:"detectflash";
this.skipDetect=deconcept.util.getRequestParameter(this.DETECT_KEY);
this.params=new Object();
this.variables=new Object();
this.attributes=new Array();
if(_1){this.setAttribute("swf",_1);}
if(id){this.setAttribute("id",id);}
if(w){this.setAttribute("width",w);}
if(h){this.setAttribute("height",h);}
if(_5){this.setAttribute("version",new deconcept.PlayerVersion(_5.toString().split(".")));}
this.installedVer=deconcept.SWFObjectUtil.getPlayerVersion();
if(c){this.addParam("bgcolor",c);}
var q=_8?_8:"high";
this.addParam("quality",q);
this.setAttribute("useExpressInstall",_7);
this.setAttribute("doExpressInstall",false);
var _d=(_9)?_9:window.location;
this.setAttribute("xiRedirectUrl",_d);
this.setAttribute("redirectUrl","");
if(_a){this.setAttribute("redirectUrl",_a);}};
deconcept.SWFObject.prototype={setAttribute:function(_e,_f){
this.attributes[_e]=_f;
},getAttribute:function(_10){
return this.attributes[_10];
},addParam:function(_11,_12){
this.params[_11]=_12;
},getParams:function(){
return this.params;
},addVariable:function(_13,_14){
this.variables[_13]=_14;
},getVariable:function(_15){
return this.variables[_15];
},getVariables:function(){
return this.variables;
},getVariablePairs:function(){
var _16=new Array();
var key;
var _18=this.getVariables();
for(key in _18){_16.push(key+"="+_18[key]);}
return _16;},getSWFHTML:function(){var _19="";
if(navigator.plugins&&navigator.mimeTypes&&navigator.mimeTypes.length){
if(this.getAttribute("doExpressInstall")){
this.addVariable("MMplayerType","PlugIn");}
_19="<embed type=\"application/x-shockwave-flash\" src=\""+this.getAttribute("swf")+"\" width=\""+this.getAttribute("width")+"\" height=\""+this.getAttribute("height")+"\"";
_19+=" id=\""+this.getAttribute("id")+"\" name=\""+this.getAttribute("id")+"\" ";
var _1a=this.getParams();
for(var key in _1a){_19+=[key]+"=\""+_1a[key]+"\" ";}
var _1c=this.getVariablePairs().join("&");
if(_1c.length>0){_19+="flashvars=\""+_1c+"\"";}_19+="/>";
}else{if(this.getAttribute("doExpressInstall")){this.addVariable("MMplayerType","ActiveX");}
_19="<object id=\""+this.getAttribute("id")+"\" classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\" width=\""+this.getAttribute("width")+"\" height=\""+this.getAttribute("height")+"\">";
_19+="<param name=\"movie\" value=\""+this.getAttribute("swf")+"\" />";
var _1d=this.getParams();
for(var key in _1d){_19+="<param name=\""+key+"\" value=\""+_1d[key]+"\" />";}
var _1f=this.getVariablePairs().join("&");
if(_1f.length>0){_19+="<param name=\"flashvars\" value=\""+_1f+"\" />";}_19+="</object>";}
return _19;
},write:function(_20){
if(this.getAttribute("useExpressInstall")){
var _21=new deconcept.PlayerVersion([6,0,65]);
if(this.installedVer.versionIsValid(_21)&&!this.installedVer.versionIsValid(this.getAttribute("version"))){
this.setAttribute("doExpressInstall",true);
this.addVariable("MMredirectURL",escape(this.getAttribute("xiRedirectUrl")));
document.title=document.title.slice(0,47)+" - Flash Player Installation";
this.addVariable("MMdoctitle",document.title);}}
if(this.skipDetect||this.getAttribute("doExpressInstall")||this.installedVer.versionIsValid(this.getAttribute("version"))){
var n=(typeof _20=="string")?document.getElementById(_20):_20;
n.innerHTML=this.getSWFHTML();return true;
}else{if(this.getAttribute("redirectUrl")!=""){document.location.replace(this.getAttribute("redirectUrl"));}}
return false;}};
deconcept.SWFObjectUtil.getPlayerVersion=function(){
var _23=new deconcept.PlayerVersion([0,0,0]);
if(navigator.plugins&&navigator.mimeTypes.length){
var x=navigator.plugins["Shockwave Flash"];
if(x&&x.description){_23=new deconcept.PlayerVersion(x.description.replace(/([a-zA-Z]|\s)+/,"").replace(/(\s+r|\s+b[0-9]+)/,".").split("."));}
}else{try{var axo=new ActiveXObject("ShockwaveFlash.ShockwaveFlash.7");}
catch(e){try{var axo=new ActiveXObject("ShockwaveFlash.ShockwaveFlash.6");
_23=new deconcept.PlayerVersion([6,0,21]);axo.AllowScriptAccess="always";}
catch(e){if(_23.major==6){return _23;}}try{axo=new ActiveXObject("ShockwaveFlash.ShockwaveFlash");}
catch(e){}}if(axo!=null){_23=new deconcept.PlayerVersion(axo.GetVariable("$version").split(" ")[1].split(","));}}
return _23;};
deconcept.PlayerVersion=function(_27){
this.major=_27[0]!=null?parseInt(_27[0]):0;
this.minor=_27[1]!=null?parseInt(_27[1]):0;
this.rev=_27[2]!=null?parseInt(_27[2]):0;
};
deconcept.PlayerVersion.prototype.versionIsValid=function(fv){
if(this.major<fv.major){return false;}
if(this.major>fv.major){return true;}
if(this.minor<fv.minor){return false;}
if(this.minor>fv.minor){return true;}
if(this.rev<fv.rev){
return false;
}return true;};
deconcept.util={getRequestParameter:function(_29){
var q=document.location.search||document.location.hash;
if(q){var _2b=q.substring(1).split("&");
for(var i=0;i<_2b.length;i++){
if(_2b[i].substring(0,_2b[i].indexOf("="))==_29){
return _2b[i].substring((_2b[i].indexOf("=")+1));}}}
return "";}};
deconcept.SWFObjectUtil.cleanupSWFs=function(){if(window.opera||!document.all){return;}
var _2d=document.getElementsByTagName("OBJECT");
for(var i=0;i<_2d.length;i++){_2d[i].style.display="none";for(var x in _2d[i]){
if(typeof _2d[i][x]=="function"){_2d[i][x]=function(){};}}}};
deconcept.SWFObjectUtil.prepUnload=function(){__flash_unloadHandler=function(){};
__flash_savedUnloadHandler=function(){};
if(typeof window.onunload=="function"){
var _30=window.onunload;
window.onunload=function(){
deconcept.SWFObjectUtil.cleanupSWFs();_30();};
}else{window.onunload=deconcept.SWFObjectUtil.cleanupSWFs;}};
if(typeof window.onbeforeunload=="function"){
var oldBeforeUnload=window.onbeforeunload;
window.onbeforeunload=function(){
deconcept.SWFObjectUtil.prepUnload();
oldBeforeUnload();};
}else{window.onbeforeunload=deconcept.SWFObjectUtil.prepUnload;}
if(Array.prototype.push==null){
Array.prototype.push=function(_31){
this[this.length]=_31;
return this.length;};}
var getQueryParamValue=deconcept.util.getRequestParameter;
var FlashObject=deconcept.SWFObject;
var SWFObject=deconcept.SWFObject;

//_________checkURL for Flash objects__________________________________________________________________________________

function checkURL(name,test){
if(test == 1){
if(document.location.href.indexOf("http")!= -1){
swfName =  name + "_web.swf";
}else{
swfName =  name + "_cd.swf";
}
}else{
swfName =  name + ".swf";
}
 return swfName;
}

    /*------------------------------------------------------------------------------
    Function:       footnoteLinks()
    Author:         Aaron Gustafson (aaron at easy-designs dot net)
    Creation Date:  8 May 2005
    Version:        1.3
    Homepage:       http://www.easy-designs.net/code/footnoteLinks/
    License:        Creative Commons Attribution-ShareAlike 2.0 License
                    http://creativecommons.org/licenses/by-sa/2.0/
    Note:           This version has reduced functionality as it is a demo of 
                    the script's development
					And Grant has added a filter so only links to http will show
    ------------------------------------------------------------------------------*/
    function footnoteLinks() {
      var containerID = 'content';
	  var targetID = 'content';
	  if (!document.getElementById || 
          !document.getElementsByTagName ||
          !document.createElement) return false;
      if (!document.getElementById(containerID) ||
          !document.getElementById(targetID)) return false;
      var container = document.getElementById(containerID);
      var target    = document.getElementById(targetID);
      var h2        = document.createElement('h2');
      addClass.apply(h2,['printOnly']);
      var h2_txt    = document.createTextNode('Links');
      h2.appendChild(h2_txt);
      var coll = container.getElementsByTagName('*');
      var ol   = document.createElement('ol');
      addClass.apply(ol,['printOnly']);
      var myArr = [];
      var thisLink;
      var num = 1;
      for (var i=0; i<coll.length; i++) {
        if ( (coll[i].getAttribute('href') && coll[i].getAttribute('href').indexOf('http')>-1) ||
             coll[i].getAttribute('cite') ) { 
          thisLink = coll[i].getAttribute('href') ? coll[i].href : coll[i].cite;
          var note = document.createElement('sup');
          addClass.apply(note,['printOnly']);
          var note_txt;
          var j = inArray.apply(myArr,[thisLink]);
          if ( j || j===0 ) { // if a duplicate
            // get the corresponding number from 
            // the array of used links
            note_txt = document.createTextNode(j+1);
          } else { // if not a duplicate
            var li     = document.createElement('li');
            var li_txt = document.createTextNode(thisLink);
            li.appendChild(li_txt);
            ol.appendChild(li);
            myArr.push(thisLink);
            note_txt = document.createTextNode(num);
            num++;
          }
          note.appendChild(note_txt);
          if (coll[i].tagName.toLowerCase() == 'blockquote') {
            var lastChild = lastChildContainingText.apply(coll[i]);
            lastChild.appendChild(note);
          } else {
            coll[i].parentNode.insertBefore(note, coll[i].nextSibling);
          }
        }
      }
      target.appendChild(h2);
      target.appendChild(ol);
      return true;
    }
	addLoadEvent(footnoteLinks);	
   
	
    /*------------------------------------------------------------------------------
    Excerpts from the jsUtilities Library
    Version:        2.1
    Homepage:       http://www.easy-designs.net/code/jsUtilities/
    License:        Creative Commons Attribution-ShareAlike 2.0 License
                    http://creativecommons.org/licenses/by-sa/2.0/
    Note:           If you change or improve on this script, please let us know.
    ------------------------------------------------------------------------------*/
    if(Array.prototype.push == null) {
      Array.prototype.push = function(item) {
        this[this.length] = item;
        return this.length;
      };
    };
    // ---------------------------------------------------------------------
    //                  function.apply (if unsupported)
    //           Courtesy of Aaron Boodman - http://youngpup.net
    // ---------------------------------------------------------------------
    if (!Function.prototype.apply) {
      Function.prototype.apply = function(oScope, args) {
        var sarg = [];
        var rtrn, call;
        if (!oScope) oScope = window;
        if (!args) args = [];
        for (var i = 0; i < args.length; i++) {
          sarg[i] = "args["+i+"]";
        };
        call = "oScope.__applyTemp__(" + sarg.join(",") + ");";
        oScope.__applyTemp__ = this;
        rtrn = eval(call);
        oScope.__applyTemp__ = null;
    	return rtrn;
      };
    };
    function inArray(needle) {
      for (var i=0; i < this.length; i++) {
        if (this[i] === needle) {
          return i;
        }
      }
      return false;
    }
    function addClass(theClass) {
      if (this.className != '') {
        this.className += ' ' + theClass;
      } else {
        this.className = theClass;
      }
    }
    function lastChildContainingText() {
      var testChild = this.lastChild;
      var contentCntnr = ['p','li','dd'];
      while (testChild.nodeType != 1) {
        testChild = testChild.previousSibling;
      } 
      var tag = testChild.tagName.toLowerCase();
      var tagInArr = inArray.apply(contentCntnr, [tag]);
      if (!tagInArr && tagInArr!==0) {
        testChild = lastChildContainingText.apply(testChild);
      }
      return testChild;
    }