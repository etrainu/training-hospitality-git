

function addLoadEvent(func) {
  var oldonload = window.onload;
  if (typeof window.onload != 'function') {
    window.onload = func;
  } else {
    window.onload = function() {
      if (oldonload) {
        oldonload();
      }
      func();
    }
  }
}

addLoadEvent(checkgoogleMaps);

function checkgoogleMaps() {
                     
 // To get all a elements in the document with a �info-links� class.
  var gmapsDivs =  getElementsByClassName(document, "div", "gmaps");
  

    
    if(gmapsDivs.length>0){    
       
        var i = new Image();
        i.onload = doConnectFunction;
        i.onerror = doNoConnectFunction;
        i.src = 'http://www.aapress.com.au/img/71f.gif?d=' + escape(Date());
        
      }
    
   
    }


    
function doConnectFunction() {  
   
}

function doNoConnectFunction() {  
 

   
    var gmapsDivs =  getElementsByClassName(document, "div", "gmaps");
    
     for (var i = 0; i < gmapsDivs.length; i++) {       

          var gDiv=gmapsDivs[i];
          
           gDiv.style.display = "none";
           var newOfflineDiv=document.createElement("div")           
           // newOfflineDiv.setAttribute("id", "divUnavailable")           
            newOfflineDiv.innerHTML="<b>Internet connection Not available</b>"
            newOfflineDiv.style.width = "300px";
            newOfflineDiv.style.height =  "300px";

          
            parentnode=gDiv.parentNode.insertBefore(newOfflineDiv, gDiv)
            
                 
            
        }
    }


function getElementsByClassName(oElm, strTagName, oClassNames){
	var arrElements = (strTagName == "*" && oElm.all)? oElm.all : oElm.getElementsByTagName(strTagName);
	var arrReturnElements = new Array();
	var arrRegExpClassNames = new Array();
	if(typeof oClassNames == "object"){
		for(var i=0; i<oClassNames.length; i++){
			arrRegExpClassNames.push(new RegExp("(^|\\s)" + oClassNames[i].replace(/\-/g, "\\-") + "(\\s|$)"));
		}
	}
	else{
		arrRegExpClassNames.push(new RegExp("(^|\\s)" + oClassNames.replace(/\-/g, "\\-") + "(\\s|$)"));
	}
	var oElement;
	var bMatchesAll;
	for(var j=0; j<arrElements.length; j++){
		oElement = arrElements[j];
		bMatchesAll = true;
		for(var k=0; k<arrRegExpClassNames.length; k++){
			if(!arrRegExpClassNames[k].test(oElement.className)){
				bMatchesAll = false;
				break;
			}
		}
		if(bMatchesAll){
			arrReturnElements.push(oElement);
		}
	}
	return (arrReturnElements)
}