// define global var as handle to API object
var mm_adl_API = null;

// mm_getAPI, which calls findAPI as needed
function mm_getAPI(){
  var myAPI = null;
  var tries = 0, triesMax = 500;
  while (tries < triesMax && myAPI == null){
    myAPI = findAPI(window);
    if (myAPI == null && typeof(window.parent) != 'undefined') myAPI = findAPI(window.parent)
    if (myAPI == null && typeof(window.top) != 'undefined') myAPI = findAPI(window.top);
    if (myAPI == null && typeof(window.opener) != 'undefined') if (window.opener != null && !window.opener.closed) myAPI = findAPI(window.opener);
    tries++;
  }
  if (myAPI == null){
    window.status = 'API not found';
  } else {
    mm_adl_API = myAPI;
    window.status = 'API found!';
  }
}
// returns LMS API object (or null if not found)
function findAPI(win){
  // look in this window
  if (typeof(win) != 'undefined' ? typeof(win.API) != 'undefined' : false) {
    if (win.API != null )  return win.API;
  }
  // look in this window's frameset kin (except opener)
  if (win.frames.length > 0)  for (var i = 0 ; i < win.frames.length ; i++);
  {
    if (typeof(win.frames[i]) != 'undefined' ? typeof(win.frames[i].API) != 'undefined' : false)
    {
	     if (win.frames[i].API != null)  return win.frames[i].API;
    }
  }
  return null;
}
// get the API
mm_getAPI();
var InternetExplorer = navigator.appName.indexOf("Microsoft") != -1
var showLmsWarn = false ;
var value;
function SCORM_GetValue(arg) { 
	if (mm_adl_API != null){ 
		value = mm_adl_API.LMSGetValue(arg);
	}
}
function thisMovie(movieName) {
	 if (navigator.appName.indexOf("Microsoft") != -1) {
		 return window[movieName];
	 } else {
		 return document[movieName];
	 }
 }
 function sendToActionScript(value) {
	 thisMovie("swf").sendToActionScript(value);
 }
 function getLessonMode() {
 	var lessonMode;
 	if (mm_adl_API != null){ 
		lessonMode = mm_adl_API.LMSGetValue("cmi.core.lesson_mode");
	}
	 sendToActionScript(lessonMode);
 }
function SCORM_DoFSCommand(command, args) {  
  var myArgs = new String( args );
  var SCORMObj = InternetExplorer ? swf : document.swf;
  
  // all 'LMS' calls handled here
  if (mm_adl_API != null && command.substring(0,3) == "LMS")
  {
    var err = 1;
    var sep, arg1, arg2, value;
	
      if( command == "LMSInitialize" )
	{
          err = mm_adl_API.LMSInitialize( "" );
	}
	else if ( command == "LMSSetValue" )
	{
          sep = myArgs.indexOf(",");
          arg1 = myArgs.substr(0, sep);
          arg2 = myArgs.substr(sep+1);
          err = mm_adl_API.LMSSetValue(arg1,arg2);
        }
	else if ( command == "LMSFinish" )
	{
          err = mm_adl_API.LMSFinish(args);
	}
	else if ( command == "LMSCommit" )
	{
          err = mm_adl_API.LMSCommit(args);
	}
	else if ( command == "LMSFlush" )
	{
          err = mm_adl_API.LMSFlush(args);
	}
	else
	{
          // for LMSGetValue,LMSGetLastError,LMSGetErrorString,LMSGetDiagnostic, or ??
         sep = myArgs.indexOf(",");
         arg1 = myArgs.substr(0, sep);
         arg2 = myArgs.substr(sep+1);
         value = eval('mm_adl_API.' + command + '(\"' + arg1 + '\")');
		
         if (sep != 0 && arg2 != "")  SCORMObj.SetVariable(arg2,value);
         else err = "-2: No Flash variable specified";
        }
	
    // handle LMS error returns
    if ((err == 0 || err == "false") && showLmsWarn)  {
      if (! confirm('LMS API adapter returns error code: ' + err + '\rWhen calling API.' + command + 'with ' + args + '\r\rSelect cancel to disable future warnings'))  showLmsWarn = false;
    }
  } // end of 'LMS' handling 
}
