/* last updated July 23 2009 */
function addLoadEvent(func) {
  var oldonload = window.onload;
  if (typeof window.onload != 'function') {
    window.onload = func;
  } else {
    window.onload = function() {
      if (oldonload) {
        oldonload();
      }
      func();
    }
  }
}

addLoadEvent(addBackIfNecessary);     

function addBackIfNecessary(){
		if(window.opener == false || window.opener == null){
                // if window.opener ==false this has not been opened via a script but only by a link to the same window.
                // this means we are can safely show the back button
                if (!document.getElementById || !document.createTextNode) {return;}  //Check for DOM support
                if (!document.getElementById("topNavLinks")) {return;}  //Check that the nav element actually exists
                var oTarget = document.getElementById("topNavLinks");
                var oSpan = document.createElement('span');
                var oLink = document.createElement('a');
                oLink.id = 'backLink'; // Give the link an id to allow styling
                oLink.href = '#'; // Make the link focusable for keyboard users
                oLink.appendChild(document.createTextNode("Back"));
                oLink.onclick = function() {history.go(-1);return false;}
                oSpan.appendChild(document.createTextNode(" | ")); 
                oSpan.appendChild(oLink); 
                oTarget.appendChild(oSpan);
                }
         else{
                if (!document.getElementById || !document.createTextNode) {return;}  //Check for DOM support
                if (!document.getElementById("topNavLinks")) {return;}  //Check that the nav element actually exists
                var oTarget = document.getElementById("topNavLinks");
                var oSpan = document.createElement('span');
                oSpan.appendChild(document.createTextNode(" (Text version) | "));
                oTarget.appendChild(oSpan); 
         }
}



function printPage(){
window.print?
window.print()
: alert('Sorry, your browser does not support this feature. Please choose print from the file menu.');
}


function checkURL(name,test){
if(test == 1){
if(document.location.href.indexOf("http")!= -1){
swfName =  name + "_web.swf";
}else{
swfName =  name + "_cd.swf";
}
}else{
swfName =  name + ".swf";
}
 return swfName;
}

