﻿package source.general
{
	import flash.display.*;
	import flash.events.*;
	import flash.media.*;
	import flash.net.*;

	public class GeneralSound
	{	
		public static var sound:Sound;
		public static var channel:SoundChannel;
		public static var sound_config:Object;
		public static var last_time:Number;
		public static var volume_control:Sprite;
		public static var sound_enabled_button:*;
		public static var sound_disabled_button:*;
		private static var function_finish:Function;

		public static function GetConfig () : Object
		{
			if (!sound_config){
				var sound_config:Object = {};
				var shared_object:SharedObject = SharedObject.getLocal ("sound_config", "/");
				if (shared_object) sound_config = shared_object.data;
				if (!sound_config.hasOwnProperty("volume")) sound_config.volume = 0.75;
				if (!sound_config.hasOwnProperty("mute")) sound_config.mute = false;}

			return sound_config;
		}
		public static function Play (url:String, volume:Number=1, fn_finish:Function=null) : void
		{
			if (IsMute()) return
			Stop ();

			function_finish = fn_finish;
			
			SoundMixer.soundTransform = new SoundTransform (GetConfig().volume);

			var request:URLRequest = new URLRequest (url);
			var context:SoundLoaderContext = new SoundLoaderContext (1000, true);

			sound = new Sound;
			sound.addEventListener (IOErrorEvent.IO_ERROR, OnIOError);
			sound.load (request, context);
			channel = sound.play();
			if (channel){
				channel.addEventListener (Event.SOUND_COMPLETE, SoundFinished);
				channel.soundTransform = new SoundTransform (volume);}

			last_time = 0;
		}
		public static function OnIOError (event:IOErrorEvent):void
		{
			trace (event.toString());  
		}

		public static function SetVolume (_volume:Number) : void
		{
			var volume:Number = Math.max (0, Math.min (1, _volume));
			SoundMixer.soundTransform = new SoundTransform (volume);
			GetConfig().volume = volume;
		}

		public static function GetVolume () : Number
		{
			return GetConfig().volume;
		}

		public static function IsMute ():Boolean
		{
			return GetConfig().mute;
		}

		public static function ToggleMute (event:Event=null):Boolean
		{
			var config:Object = GetConfig();
			config.mute = !config.mute;
			if (config.mute) Stop();

			if (volume_control){
				volume_control.visible = !volume_control.visible;}

			if (sound_enabled_button){
				sound_enabled_button.visible = !sound_enabled_button.visible;
				sound_disabled_button.visible = !sound_disabled_button.visible;}

			return config.mute;
		}

		public static function GetPlayTime () : Number
		{
			if (sound && channel)
				return channel.position / 1000;
			return 0;
		}

		public static function GetPlayTimeDifferenceString () : String
		{
			var play_time:Number = GetPlayTime();
			var time_difference:Number = play_time - last_time;
			var time_string:String = time_difference.toFixed(1);
			if (time_string.slice(-2) == ".0") time_string = time_string.slice (0,-2);
			last_time = play_time;
			return time_string;
		}

		public static function SetupToggleButtons ():void
		{
			//sound_enabled_button.buttonMode = true;
			sound_disabled_button.buttonMode = true;
			sound_enabled_button.addEventListener (MouseEvent.CLICK, ToggleMute);
			sound_disabled_button.addEventListener (MouseEvent.CLICK, ToggleMute);
			sound_enabled_button.visible = !(sound_disabled_button.visible = IsMute());
		}

		public static function DrawVolumeControl (parent:*):void
		{
			volume_control = new Sprite;
			volume_control.addEventListener (MouseEvent.MOUSE_DOWN, MouseChangeVolume);
			volume_control.addEventListener (MouseEvent.MOUSE_MOVE, MouseChangeVolume);
			parent.addChild (volume_control);
			DrawInnerVolume ();

			if (GetConfig().mute)
				volume_control.visible = false;
		}

		public static function DrawInnerVolume ():void
		{
			var g:Graphics = volume_control.graphics;
			g.clear();
			g.beginFill(0,0);
			g.drawRect (-2,0,56,16);
			g.endFill();
			for (var i:int=0; i<12; i++){
				var j:int = i+2;
				var factor:Number = Math.sqrt (GetConfig().volume);
				if (i < factor*14) g.beginFill (0x888888);
				else g.beginFill (0xD0D0D0);
				g.drawRect (i*4, 15-j, 3, j);
				g.endFill();}
		}

		public static function DrawInnerVolumeShort ():void
		{
			var g:Graphics = volume_control.graphics;
			g.clear();
			g.beginFill(0,0);
			g.drawRect (-2,0,28,16);
			g.endFill();
			for (var i:int=0; i<6; i++){
				var j:int = i+2;
				var factor:Number = Math.sqrt (GetConfig().volume);
				if (i < factor*7) g.beginFill (0x6D6F71);
				else g.beginFill (0xC2C4C6);
				g.drawRect (i*4, 15-j*2, 3, j*2);
				g.endFill();}
		}

		public static function MouseChangeVolume (event:MouseEvent):void
		{
			if (event.buttonDown){
				var config:Object = GetConfig();
				var sprite:Sprite = Sprite (event.currentTarget);
				var factor:Number = Math.min (Math.max (0, event.localX / sprite.width), 1);
				SetVolume (factor * factor); // non-linear volume slider is better
				DrawInnerVolume ();}
		}

		public static function SoundFinished (event:Event):void
		{
			if (function_finish != null)
				function_finish ();
		}

		public static function Stop () : void
		{
			if (sound){
				if (channel){
					try { channel.removeEventListener (Event.SOUND_COMPLETE, SoundFinished); channel.stop();} catch (e:*){};}
				try { sound.close();} catch (e:*){};
				sound.removeEventListener (IOErrorEvent.IO_ERROR, OnIOError);
				channel = null;
				sound = null;}

			function_finish = null;
		}
	}
}
