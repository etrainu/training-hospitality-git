package source.custom
{
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	public dynamic class CustomEventDispatcher extends EventDispatcher
	{
		public function CustomEventDispatcher(target:IEventDispatcher=null)
		{
			super(target);
		}
	}
}