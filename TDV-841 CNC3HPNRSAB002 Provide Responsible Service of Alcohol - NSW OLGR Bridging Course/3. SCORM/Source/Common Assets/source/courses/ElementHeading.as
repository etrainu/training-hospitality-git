package source.courses
{
	import flash.display.*;
	import source.general.*;
	import source.custom.*;

	public class ElementHeading extends Element
	{
		public static var style_array:Array = ["large","medium","italic","centered","small"];
		public static var text_size_array:Array = [24,20,20,20,16];
		public static var text_color_array:Array = [0x909090,0x707070,0x707070,0x707070,0x505050];
		public static var margin_top_array:Array = [20,14,14,14,10];
		public static var margin_bottom_array:Array = [6,6,6,6,4];
		public var style_index:int = 1;

		public override function Setup (_xml:XML, _slide:CourseSlide):void
		{
			super.Setup (_xml, _slide);

			var style:String = _xml.@style;
			style_index = style_array.indexOf (style);
			if (style_index == -1) style_index = 1; // default medium

			element_margin_top = margin_top_array[style_index];
			element_margin_bottom = margin_bottom_array[style_index];

			is_text = true;
			variable_width = true;
			is_graphical = true;
			allow_timing = true;
			allow_filter = true;
		}
		public override function DoLayout (layout_width:int, layout_height:int, fix_width:Boolean=false, fix_height:Boolean=false):Boolean
		{
			if (element_sprite){
				GeneralText.ResizeText (CustomSprite(Object(element_sprite).text), layout_width);
				element_width = element_sprite.width;
				element_height = element_sprite.height;
				return true;}

			element_sprite = new CustomSprite;
			var style:String = String(xml.@style) + ",bold";
			var heading_text:String = ResolveText();
			var heading_text_sprite:Sprite = GeneralText.MakeText (heading_text, text_size_array[style_index], text_color_array[style_index], 0, 0, fix_width ? layout_width : -layout_width, null, style);
			Object(element_sprite).text = heading_text_sprite;
			element_sprite.addChild (heading_text_sprite);

			// Do long underline for H1
			if (style_index == 0){
				var underline:Sprite = new Sprite;
				var underline_y:int = heading_text_sprite.height;
				underline.graphics.lineStyle (1, text_color_array[style_index])
				underline.graphics.moveTo (0, underline_y);
				underline.graphics.lineTo (layout_width - 8, underline_y);
				element_sprite.addChild (underline);}

			element_width = element_sprite.width;
			element_height = element_sprite.height;
			return true;
		}
		public override function DocumentElement ():String
		{
			var h:int = style_index + 1
			var text:String = xml.text;
			return "<h" + h + ">" + text + "</h" + h + ">";
		}
	}
}
