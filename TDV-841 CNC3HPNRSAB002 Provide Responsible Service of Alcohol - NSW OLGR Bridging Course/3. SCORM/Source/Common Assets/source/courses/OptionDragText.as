package source.courses
{
	import flash.display.*;
	import flash.events.*;
	import flash.text.*;
	import source.custom.*;
	import source.general.*;

	public class OptionDragText extends OptionDrag
	{
		public function OptionDragText (xml:XML, down:Function, move:Function, drop:Function, text_width_max:int)
		{
			var box : Sprite = new Sprite;
			var text:String = GeneralString.Resolve (xml.text);
			var text_sprite:CustomSprite = GeneralText.MakeText (text, text_size, text_color, 0, 0);

			if (text_sprite.width > text_width_max){
				text_sprite = GeneralText.MakeText (text, text_size, text_color, 0, 0, text_width_max);}

			text_sprite.x = horz_margin;
			text_sprite.y = vert_margin;

			var box_width:int = text_sprite.width + horz_margin * 2;
			var box_height:int = text_sprite.height + vert_margin * 2;

			super (xml, box_width, box_height, down, move, drop);

			box.graphics.lineStyle (1, border_color, border_alpha, true);
			box.graphics.beginFill (background_color, background_alpha);
			box.graphics.drawRoundRect (0, 0, box_width, box_height, corner_radius, corner_radius);
			box.buttonMode = true;
			box.addChild (text_sprite);
			this.addEventListener (MouseEvent.MOUSE_DOWN, OnMouseDown);
			addChild (box);
		}
	}
}
