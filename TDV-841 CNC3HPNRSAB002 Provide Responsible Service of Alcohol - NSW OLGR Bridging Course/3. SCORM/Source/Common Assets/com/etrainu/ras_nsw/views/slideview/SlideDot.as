package com.etrainu.ras_nsw.views.slideview 
{
	import flash.display.Sprite;
	import flash.events.Event;
	import com.greensock.TweenLite;
	import com.greensock.TweenMax;
	import com.greensock.easing.*;
	/**
	 * ...
	 * @author ...
	 */
	public class SlideDot extends Sprite 
	{
		private const SELECTED_COLOR:Number = 0x4D4C42;
		private var _selected:Boolean;
		private const delayStep:Number = 0.2;
		private var delay:Number;
		public function SlideDot(delayFactor:uint = 0) 
		{
			delay = delayFactor * delayStep;
			addEventListener(Event.ADDED_TO_STAGE, onAdded, false, 0, true);
			_selected = false;
		}
		private function onAdded(event:Event):void {
			removeEventListener(Event.ADDED_TO_STAGE, onAdded);
			TweenLite.from(this, 1, { alpha:0, scaleX:0.2, scaleY:0.2, ease:Back.easeOut, onComplete:selectify, delay:delay} );
		}
		private function selectify():void {
			if (_selected) {
				TweenMax.to(this, 1.5, { colorMatrixFilter:{colorize:SELECTED_COLOR, amount:1}, ease:Expo.easeOut} );
			}else {
				TweenMax.to(this, 1.5, { colorMatrixFilter:{colorize:SELECTED_COLOR, amount:0}, ease:Expo.easeOut} );
			}
		}
		public function get selected():Boolean 
		{
			return _selected;
		}
		
		public function set selected(value:Boolean):void 
		{
			_selected = value;
		}
		
	}

}