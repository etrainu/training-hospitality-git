﻿package Utils{

	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.display.DisplayObject; 
	import flash.events.EventDispatcher;
	
	import flash.utils.Timer;
    import flash.events.TimerEvent;

	import com.greensock.TweenLite;
	import com.greensock.easing.*;
	//import fl.controls.TextLayout;
	
	public class DoteesHint extends EventDispatcher {

		// Constants:
		private static const cursorHeight:int = 20;
		private static const cursorWidth:int = 20;
		// Public Properties:

		// Private Properties:
		private static var hintHolder:Sprite;
		private static var textContainer:TootipView;
		private static var _bg:hintBG;
		private static var offTimer:Timer = new Timer(1);
		private static var global:Global;
		
		private static var instance:DoteesHint = null;
		private static var allowInstantiation:Boolean = false;
		
		
		// UI Elements:

		// Initialization:
		public function DoteesHint() {
			global = Global.getInstance();
			textContainer = new TootipView();
		}
		public static function getInstance():DoteesHint {
			if ( DoteesHint.instance == null ) {
				DoteesHint.allowInstantiation = true;
				DoteesHint.instance = new DoteesHint();
				DoteesHint.allowInstantiation = false;
			}
			return DoteesHint.instance;
		}
		
		// Public Methods:
		public static function attach(hintStr:String , duration:int = 8, maxWidth:uint = 300):void {
			trace("global.root: " + global.root);
			//trace("step 1");
			if (offTimer) {
				offTimer.stop();
				offTimer.removeEventListener("timer", timerHandler);
				TweenLite.killTweensOf(textContainer, true);
			}
			offTimer = new Timer(duration * 1000, 1);
			offTimer.addEventListener("timer", timerHandler);
			offTimer.start();
			textContainer.maxWidth = maxWidth;
			textContainer.text = hintStr;
			//trace("step 2");
			if (global.root.mouseX + cursorWidth + textContainer.width <= global.root.stage.stageWidth) {
				//trace("step 3");
				textContainer.x = global.root.mouseX + cursorWidth;
			}else {
				//trace("step 4");
				textContainer.x = Math.max(global.root.mouseX - textContainer.width, 10);
			}
			//trace("step 5");
			if (global.root.mouseY + cursorHeight + textContainer.height <= global.root.stage.stageHeight) {
				//trace("step 6");
				textContainer.y = global.root.mouseY + cursorHeight;
			}else {
				//trace("step 7");
				textContainer.y = Math.max(global.root.mouseY - textContainer.height, 10);
			}
			textContainer.cacheAsBitmap = true;
			global.root.addChild(textContainer);
		}
		public static function remove():void {
			offTimer.stop();
			TweenLite.killTweensOf(textContainer, true);
			TweenLite.to(textContainer, 0.5, { alpha:0, ease:Quad.easeOut, onComplete:onFadeOut});
			offTimer.removeEventListener("timer", timerHandler);
		}
		// Protected Methods:
		// Private Methods:
		private static function timerHandler(event:TimerEvent):void {
			TweenLite.killTweensOf(textContainer, true);
			TweenLite.to(textContainer, 0.5, { alpha:0, ease:Quad.easeOut, onComplete:onFadeOut } );
			offTimer.stop();
			offTimer.removeEventListener("timer", timerHandler);
        }
		private static function onFadeOut():void {
			if(textContainer.stage){
				global.root.removeChild(textContainer);
				textContainer.alpha = 1;
			}
		}
		protected function configUI():void {
		}
	}

}