﻿package Utils 
{
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	
	import Utils.*;
	/**
	 * ...
	 * @author Amir Zahedi
	 */
	public class ToolTips 
	{
		private static var global:Global = Global.getInstance();
		public function ToolTips() 
		{
			
		}
		public static function addInstance(instance:DisplayObject):void {
			instance.addEventListener(MouseEvent.ROLL_OVER, onMouseOver);
			instance.addEventListener(MouseEvent.ROLL_OUT, onMouseOut);
		}
		private static function onMouseOver(event:MouseEvent):void {
			DoteesHint.attach(event.target.description);
		}
		private static function onMouseOut(event:MouseEvent):void {
			DoteesHint.remove();
		}
	}
	
}